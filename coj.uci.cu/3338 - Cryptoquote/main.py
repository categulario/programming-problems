import sys

if __name__ == '__main__':
    cases = int(sys.stdin.readline().strip())

    for i in range(1, cases+1):
        message = sys.stdin.readline().strip()
        key     = sys.stdin.readline().strip()

        sys.stdout.write("%d "%i)

        for c in map(ord, message):
            if 65<=c<=90:
                sys.stdout.write(key[c-65])
            else:
                sys.stdout.write(chr(c))
        sys.stdout.write('\n')
