# Fraction result

## Description

Given three numbers a, b and c you should find the sum of the first c digits of the result of the fraction a / b, and the cth digit. Knowing that the digits are indexed from 1.
Input specification
The input consists of several lines, each with three integers a, b and c (| a | <= 100000, | b | <= 100000, b ≠ 0, 0 < c <= 10 ^ 15) separated by a space.

## Output specification

For each input line should be printed a line with two numbers separated by a space.

## Sample input

```
3 2 4
```

## Sample output

```
6 0
```
