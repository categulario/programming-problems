#include <iostream>

using namespace std;

int main() {
	bool moduli[] = {false, false, false, false, false, false, false, false,
		false, false, false, false, false, false, false, false, false, false,
		false, false, false, false, false, false, false, false, false, false,
		false, false, false, false, false, false, false, false, false, false,
		false, false, false, false};
	int num, count=0, mod;

	for (int i=0; i<10; i++) {
		cin >> num;
		mod = num%42;
		if(!moduli[mod]) {
			moduli[mod] = true;
			count++;
		}
	}

	cout << count << endl;

}
