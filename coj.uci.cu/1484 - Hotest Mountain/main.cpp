#include <iostream>

using namespace std;

int main() {
	int mountains, pos=0;
	float height=0, highest=0;

	cin >> mountains;

	for (int i=0; i<mountains; i++) {
		cin >> height;
		if (height >= highest) {
			highest = height;
			pos = i;
		}
	}

	cout << pos+1 << endl;

	return 0;
}
