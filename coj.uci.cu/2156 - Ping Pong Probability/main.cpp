#include<stdio.h>
#include<math.h>

float prob_of_winning(int nth, float p) {
    if (nth == 1) {
        return p;
    }

    float prob_of_starting = 1 - prob_of_winning(nth-1, p);

    return prob_of_starting*p + (1-prob_of_starting)*(1-p);
}

int main() {
    int t, n;
    float p;

    scanf("%d", &t);

    for (int i=0; i<t; i++) {
        scanf("%d %f", &n, &p);
        printf("%.5f\n", prob_of_winning(n, p));
    }
}
