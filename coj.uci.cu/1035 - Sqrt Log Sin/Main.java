import java.util.Scanner;

class Main {
	static int store[] = new int[10000001];

	public static int seq(int i) {
		// calcula el i-esimo término de la sucesión
		if (store[i] == 0) {
			store[i] = (seq((int)Math.floor(i - Math.sqrt(i))) + seq((int)Math.floor(Math.log(i))) + seq((int)Math.floor(i*Math.pow(Math.sin(i), 2))))%1000000;
		}

		return store[i];
	}

	public static void main(String args[]) {
		Scanner entrada = new Scanner(System.in);
		int numero;

		store[0] = 1;
		for (int i=1; i<10000001; i++) {
			store[i] = 0;
		}

		while(true) {
			numero = entrada.nextInt();

			if(numero < 0)
				break;

			System.out.println(seq(numero));
		}
	}
}
