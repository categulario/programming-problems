#include <cmath>
#include <iostream>
#include <cstring>
#include <limits>

using namespace std;

const int N = 1000000;
const int N2 = 1000001;

int seq[N];
int sqrts[N];
int logs[N];
int sins[N];

void init() {
	for (int i=1; i<N; ++i) {
		sqrts[i] = floor(i - sqrt(i));
		logs[i]  = floor(log(i));
		sins[i]  = floor(i * pow(sin(i), 2));
	}
}

int compute_seq(int i) {
	if (seq[i] != 0) {
		return seq[i];
	}
	return seq[i] = (compute_seq(sins[i]) + compute_seq(sqrts[i]) + compute_seq(logs[i]))%N;
}

int main() {
	init();
	memset(seq, 0, N);
	seq[0] = 1;
	int num;
	while (true) {
		cin >> num;
		if (num == -1) {
			break;
		}
		cout << compute_seq(num) << endl;
	}
	return 0;
}
