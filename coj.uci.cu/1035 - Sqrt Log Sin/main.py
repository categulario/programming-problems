from math import sqrt, log, sin, floor
import sys

values = {
    0: 1
}
sums = {} # parent -> accumulated sum

def seq(num):
    queue = [(num, None)]

    while len(queue) > 0:
        # print(queue)
        i, parent = queue.pop()

        if i not in values:
            # return this to the queue
            queue.append((i, parent))

            # lets calculate the value
            queue.append((floor(i-sqrt(i)), i))
            queue.append((floor(log(i)), i))
            queue.append((floor(i*sin(i)**2), i))
        else:
            if parent in sums:
                sums[parent]['sum'] += values[i]
                sums[parent]['count'] += 1
            else:
                sums[parent] = {'sum': values[i], 'count': 1}

            if sums[parent]['count'] == 3:
                values[parent] = sums[parent]['sum']
                del sums[parent]

    return values[num]

if __name__ == '__main__':
    while True:
        num = int(sys.stdin.readline().strip())
        if num == -1:
            break
        print(seq(num))
