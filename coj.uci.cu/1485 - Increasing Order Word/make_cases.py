import sys

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("missing argument")
        exit(0)
    word = sys.argv[1]
    with open('in.txt', 'w') as inputfile:
        inputfile.write(word+'\n')
    sortedword = ''.join(sorted(word))
    with open('out.txt', 'w') as outputfile:
        outputfile.write(sortedword+'\n')
