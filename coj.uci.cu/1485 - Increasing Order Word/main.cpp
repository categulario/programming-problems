#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int choose_pivot(char *array, int a, int c) {
	int b = a+1;

	if (array[a] < array[b] && array[a] < array[c]) {
		if (array[b] < array[c])
			return b;
		else
			return c;
	}
	if (array[b] < array[a] && array[b] < array[c]) {
		if (array[a] < array[c])
			return a;
		else
			return c;
	}
	if (array[c] < array[b] && array[c] < array[a]) {
		if (array[b] < array[a])
			return b;
		else
			return a;
	}
	return b;
}

void print_array(char *array, int left, int right) {
	for (int i=left; i<=right; i++) {
		cout << array[i];
	}
	cout << endl;
}

void quicksort(char *array, int left, int right) {
	int diff = right - left;
	if (diff == 0) {
		// the same element, this should not happen
		return;
	} else if (diff == 1) {
		// only two elements, swap if needed
		if (array[left] > array[right]) {
			swap(array[left], array[right]);
		}
		return;
	}

	// now lets choose a pivot
	int pivot = choose_pivot(array, left, right);
	int value = array[pivot];
	int i=left, j=right;

	while ( i < j ) {
		while (i<j && array[i] < value)
			i++;
		while (j>i && array[j] > value)
			j--;
		if (array[i] > array[j]) {
			swap(array[i], array[j]);
			if (i == pivot)
				pivot = i;
			if (j == pivot)
				pivot = j;
		} else if (j != pivot) {
			j--;
		} else if (i != pivot) {
			i++;
		} else {
			break;
		}
	}

	if (i-1 > left)
		quicksort(array, left, i-1);
	if (i+1 < right)
		quicksort(array, i+1, right);
}

int main() {
	char string[1000];

	cin >> string;

	quicksort(string, 0, strlen(string)-1);

	cout << string << endl;

	return 0;
}
