#include<iostream>

using namespace std;

int main() {
    int n, squares, rectangles, positions;

    while (cin >> n) {
        squares = 0;
        rectangles = 0;

        for (int i=1; i<=n; i++) {
            for (int j=1; j<=n; j++) {
                positions = (n - i + 1) * (n - j + 1);

                rectangles += positions;

                if (i == j) {
                    squares += positions;
                }
            }
        }

        cout << squares << ' ' << rectangles << endl;
    }
}
