# Numeric center I

## Description

A numeric center is a number that separates in a consecutive and positive integer number list (starting at one) in two groups of consecutive and positive integer numbers, in which their sum is the same. The first numeric center is number 6, which takes the list {1, 2, 3, 4, 5, 6, 7, 8 } and produces two lists of consecutive and positive integer numbers in which their sum (in this case 15) is the same. Those lists are: {1, 2, 3, 4, 5} and {7, 8}. The second numeric center is 35, that takes the list {1, 2, 3, 4, ..., 49} and produces the following two lists: {1, 2, 3, 4, ..., 34} and {36, 37, 38, 39, ..., 49}, the sum of each list is equal to 595.

The task consists in writing a program that calculates the total of numeric centers between 1 and n.

## Input spec

The input consists of several test cases (but no more than 1000000). There is only one line for each test case. This line contains a positive integer number n (1 <= n <= 10^6). The last test case is a value of n equal to zero, this test case should not be processed.

## Output spec

For each test case you have to print in one line, the number of numeric centers between 1 and n.

## Sample input

```
1
7
8
48
49
50
0
```

## Sample output

```
0
0
1
1
2
2
```

## Hint

Use faster I/O methods 
