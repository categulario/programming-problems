#include <iostream>

using namespace std;

int main() {
	int num;

	cin >> num;

	int res = num%3;
	int q   = num/3;

	int add = 0;

	if (res == 2)
		add = 1;
	else if (res == 0)
		add = 2;

	cout << q*2 + add << endl;

	return 0;
}
