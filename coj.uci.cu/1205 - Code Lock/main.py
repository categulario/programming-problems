import sys

if __name__ == '__main__':
    while True:
        string = sys.stdin.readline().strip()
        if string == '*':
            break

        distinct = 0
        cur = ''

        for i in string:
            if i!=cur:
                if i != 'a':
                    distinct += 1
                cur = i

        print(distinct)
