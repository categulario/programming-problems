#include <iostream>

using namespace std;

int sumsubseq(int a, int b) {
    int sum = 0;

    for (int i=a; i<=b; i++) {
        sum = (sum + i*(i+1)*(i+2)) % 100;
    }

    return sum;
}

int main() {
    int cases, a, b;
    cin >> cases;

    while (cases--) {
        cin >> a >> b;
        cout << sumsubseq(a, b) << endl;
    }
}
