#include <stdio.h>

long tabla[10000001];

int main() {
    int t, k;
    int n;
    int i;

    scanf("%d %d", &t, &k);

    // inicializacion de la tabla
    for (i = 0; i < 10000001; i++) {
        if (i == 0) {
            tabla[i] = 0;
            continue;
        }

        if (i >= 1 && i < k) {
            tabla[i] = 1;
            continue;
        }

        tabla[i] = 0;

        for (int j = 1; j <= k; j++) {
            tabla[i] += tabla[i-j];
        }
    }

    for (i = 0; i < t; i++) {
        scanf("%d", &n);

        printf("%ld\n", tabla[n] % 1000000007);
    }
}
