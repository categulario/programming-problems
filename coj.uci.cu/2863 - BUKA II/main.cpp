#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int min(int a, int b) {
	return (a + b)/2 - abs(a-b)/2;
}

int main() {
	char c, oper;
	int digits1=0, digits2=0, i;

	scanf(" %c", &c);
	scanf(" %c", &c);

	while (c == '0') {
		digits1++;
		scanf(" %c", &c);
	}

	oper = c;

	scanf(" %c", &c); // skip number one
	scanf(" %c", &c); // take first 0

	while (c == '0') {
		digits2++;
		c = getchar();
	}

	if (oper == '+') {
		if (digits1 == digits2) {
			putchar('2');
			for (i=0; i<digits1; i++) {
				putchar('0');
			}
		} else {
			putchar('1');
			for (i=0; i<abs(digits1-digits2)-1; i++) {
				putchar('0');
			}
			putchar('1');
			for (i=0; i<min(digits1, digits2); i++) {
				putchar('0');
			}
		}
	} else if (oper == '-') {
		if (digits1 != digits2) {
			if (digits1 < digits2) {
				putchar('-');
			}
			for (i=0; i<abs(digits1-digits2); i++) {
				putchar('9');
			}
			for (i=0; i<min(digits1, digits2); i++) {
				putchar('0');
			}
		} else {
			putchar('0');
		}
	} else if (oper == '*') {
		putchar('1');
		for (i=0; i<digits1+digits2; i++) {
			putchar('0');
		}
	} else if (oper == '/') {
		if (digits1 >= digits2) {
			putchar('1');
			for (i=0; i<digits1-digits2; i++) {
				putchar('0');
			}
		} else {
			putchar('0');
			putchar('.');
			for (i=0; i<digits2-digits1-1; i++) {
				putchar('0');
			}
			putchar('1');
		}
	} else { // % moduli
		if (digits1 >= digits2) {
			putchar('0');
		} else {
			putchar('1');
			for (i=0; i<digits1; i++) {
				putchar('0');
			}
		}
	}
	putchar('\n');
}
