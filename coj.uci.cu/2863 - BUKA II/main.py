import sys

if __name__ == '__main__':
    digits1 = 0
    digits2 = 0

    c = sys.stdin.read(1)

    while sys.stdin.read(1) == '0':
        digits1+=1

    c = sys.stdin.read(1) # skip newline

    oper = c

    sys.stdin.read(1) # skip newline
    sys.stdin.read(1) # skip number one

    while sys.stdin.read(1) == '0':
        digits2+=1

    if oper == '+':
        if digits1 == digits2:
            sys.stdout.write('2')
            for i in range(digits1):
                sys.stdout.write('0')
        else:
            sys.stdout.write('1')
            for i in range(abs(digits1-digits2)-1):
                sys.stdout.write('0')
            sys.stdout.write('1')
            for i in range(min(digits1, digits2)):
                sys.stdout.write('0')
    elif oper == '-':
        if digits1 != digits2:
            if digits1 < digits2:
                sys.stdout.write('-')
            for i in range(abs(digits1-digits2)):
                sys.stdout.write('9')
            for i in range(min(digits1, digits2)):
                sys.stdout.write('0')
        else:
            sys.stdout.write('0')
    elif oper == '*':
        sys.stdout.write('1')
        for i in range(digits1+digits2):
            sys.stdout.write('0')
    elif oper == '/':
        if digits1 >= digits2:
            sys.stdout.write('1')
            for i in range(digits1-digits2):
                sys.stdout.write('0')
        else:
            sys.stdout.write('0.')
            for i in range(digits2-digits1-1):
                sys.stdout.write('0')
            sys.stdout.write('1')
    else: # % moduli
        if digits1 >= digits2:
            sys.stdout.write('0')
        else:
            sys.stdout.write('1')
            for i in range(digits1):
                sys.stdout.write('0')
    sys.stdout.write('\n')
