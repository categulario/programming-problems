# Buka II

# Description

Frequently, there is substantial noise in the classroom during class. Instead of paying attention to what the teacher is saying, the students rather discuss the economic crisis or Croatia's joining the European Union.

The biggest noise often occurs when the students are idle, so teachers will give them tasks with a lot of work to calm them down. In one type of such task, the student must calculate the result of an arithmetic operation on two large numbers.

The arithmetic operations we will consider are adding (+), subtraction (-), multiplication (*), division (/) and remainder (%). The operands will be powers of 10 with no more than 10^6 digits. Write a program that calculates the result of the operation.

## Input spec

The first line contains a positive integer A, the first operand. The second line contains the operator of an operation, '+' or '*' or '/' or '-' or '%'. The third line contains a positive integer B, the second operand.

## Output spec

Output the result of the operation.

## Sample input

```
1
+
1
```

## Sample output

```
2
```
