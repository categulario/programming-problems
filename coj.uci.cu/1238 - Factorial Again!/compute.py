''' computes the table '''
from pprint import pprint


def factorial(k):
    if k == 1 or k == 0:
        return 1

    return k * factorial(k-1)


pprint([
    [
        d*factorial(k+1) for d in range(10)
    ] for k in range(5)
])
