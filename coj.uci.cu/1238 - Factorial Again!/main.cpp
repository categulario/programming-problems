#include<stdio.h>

using namespace std;

const int POSSIBLE_RESULTS = 100000;

int table[5][10] = {
    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
    {0, 2, 4, 6, 8, 10, 12, 14, 16, 18},
    {0, 6, 12, 18, 24, 30, 36, 42, 48, 54},
    {0, 24, 48, 72, 96, 120, 144, 168, 192, 216},
    {0, 120, 240, 360, 480, 600, 720, 840, 960, 1080},
};
int results[POSSIBLE_RESULTS];

int compute(int n) {
    if (results[n] != 0) {
        return results[n];
    }

    int k = 0;
    int ai, result=0;
    int bck = n;

    while (n > 0) {
        ai = n%10;
        result += table[k][ai];
        n /= 10;
        k ++;
    }

    results[bck] = result;

    return result;
}

int main() {
    long n, result;
    int k;
    int ai;

    while (true) {
        scanf("%ld", &n);

        if (n == 0) {
            break;
        }

        result = compute(n);

        //cout << result << endl;
        printf("%d\n", result);
    }
}
