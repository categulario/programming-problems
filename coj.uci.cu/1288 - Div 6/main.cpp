#include <iostream>
#include <string.h>

using namespace std;

int main() {
	int test_cases;
	char num[1000];
	int digits;
	int sum = 0;
	cin >> test_cases;
	for (int i=0; i<test_cases; i++) {
		cin >> num;
		digits = strlen(num);

		for (int j=0; j<digits; j++) {
			sum = (sum + (int)num[j]-48)%3;
		}

		if (sum%3 == 0 && (int)num[digits-1]%2==0) {
			cout << "YES" << endl;
		} else {
			cout << "NO" << endl;
		}

		sum = 0;
	}
	return 0;
}
