import sys

cases = int(input())

while cases:
    lastchar = sys.stdin.readline().strip()[-1]

    if lastchar == '5' or lastchar == '0':
        print('YES')
    else:
        print('NO')

    cases -= 1
