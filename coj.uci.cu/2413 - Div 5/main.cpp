#include<iostream>
#include<stdio.h>

using namespace std;

int main() {
    int n;
    char tmp, c;

    cin >> n;
    getchar();

    while (n--) {
        do {
            c = tmp;
            tmp = getchar();
        } while (tmp != '\n');

        if (c == '5' || c == '0') {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }
}
