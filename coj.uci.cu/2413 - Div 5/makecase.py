from random import randint, choice
import sys

t = randint(1, 100)

print(t)

for _ in range(t):
    num = ''.join(choice('0123456789') for _ in range(randint(1, 1000)))

    print(num)
    print('YES' if num.endswith('5') or num.endswith('0') else 'NO', file=sys.stderr)
