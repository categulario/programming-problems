#include<iostream>
#include <string>

using namespace std;

int main () {
    int n;

    cin >> n;

    while (n--) {
        string line;
        getline(cin, line);

        if (line[line.length()] == '0' || line[line.length()] == '5') {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }
}
