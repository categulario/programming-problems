#include <stdio.h>

long apply(long a, long b, char sym) {
    if (sym == '+') {
        return a + b;
    } else if (sym == '*') {
        return a * b;
    } else {
        printf("todo está mal\n");
        return 0;
    }
}

long solvesubcase(int* three, char* sym) {
    long min = 9223372036854775807;

    for (int i = 0; i < 2; i++) {
        for (int j = i+1; j < 3; j++) {
            int two[2];

            two[0] = apply(three[i], three[j], sym[0]);

            for (int k = 0; k < 3; k++) {
                if (k != i && k != j) {
                    two[1] = three[k];
                }
            }

            long res = apply(two[0], two[1], sym[1]);

            if (res < min) {
                min = res;
            }
        }
    }

    return min;
}

long solvecase(int* arr, char* sym) {
    long min = 9223372036854775807;

    for (int i = 0; i < 3; i++) {
        for (int j = i+1; j < 4; j++) {
            int three[3];

            three[0] = apply(arr[i], arr[j], sym[0]);

            for (int k = 0, p = 1; k < 4; k++) {
                if (k != i && k != j) {
                    three[p] = arr[k];
                    p++;
                }
            }

            long sub = solvesubcase(three, sym+1);

            if (sub < min) {
                min = sub;
            }
        }
    }

    return min;
}

int main() {
    int cases;
    int arr[4];
    char symbols[3];

    scanf("%d", &cases);

    for (int i = 0; i < cases; i++) {
        scanf("%d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3]);
        getchar(); // skip newline

        for (int j = 0; j < 3; j++) {
            symbols[j] = getchar();

            if (j != 2) {
                getchar();
            }
        }

        printf("%ld\n", solvecase(arr, symbols));
    }
}
