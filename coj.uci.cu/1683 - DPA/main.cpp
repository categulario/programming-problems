#include<iostream>

using namespace std;

int main() {
    int n, c;
    int primes[] = {2, 3, 5, 7, 11, 13, 17, 19};
    int prime_count = 8;
    int divisor_sum;

    cin >> n;

    while (n--) {
        cin >> c;
        divisor_sum = 0;

        for (int i=1; i<=c/2; i++) {
            if (c%i == 0) {
                divisor_sum += i;
            }
        }

        if (divisor_sum < c) {
            cout << "Deficient" << endl;
        } else if (divisor_sum == c) {
            cout << "Perfect" << endl;
        } else {
            cout << "Abundant" << endl;
        }
    }
}
