#include<iostream>
#include<stdio.h>

using namespace std;

int main() {
    string s;
    int n; // number of groups
    int size;
    char tmp;

    while (true) {
        scanf("%d ", &n);

        if (n == 0) {
            break;
        }

        cin >> s;

        size = s.length()/n;

        for (int i=0; i<n; i++) {
            for (int j=i*size,k=(i+1)*size-1; j<k; j++,k--) {
                tmp = s[j];
                s[j] = s[k];
                s[k] = tmp;
            }
        }

        cout << s << endl;
        s.clear();
    }
}
