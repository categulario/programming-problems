#include <iostream>

using namespace std;

int binary(int num) {
	int residue;
	int retval = 0;

	if (num<=1) {
		cout << num;
		return num;
	}

	residue = num%2;
	retval = residue + binary(num>>1);
	cout << residue;

	return retval;
}

void print_result(int num) {
	cout << " is " << binary(num);
}

int main() {
	int num;
	while (true) {
		cin >> num;
		if (num == 0) {
			break;
		}
		cout << "The parity of ";
		print_result(num);
		cout << " (mod 2)." << endl;
	}
	return 0;
}
