#include <iostream>

using namespace std;

int compute(int n) {
	return (n*(n+1)*(2*n+1))/6;
}

int main() {
	int n;
	while (true) {
		cin >> n;
		if (n==0)
			break;
		cout << compute(n) << endl;
	}
}
