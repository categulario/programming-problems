#include <iostream>

using namespace std;

int main() {
	int test_cases, rows, cols;
	cin >> test_cases;
	char res[] = {'R', 'D', 'L', 'U'};

	while (test_cases-- > 0) {
		cin >> rows >> cols;

		if (rows > cols + 1) {
			rows = cols + 1;
		} else if (cols > rows) {
			cols = rows;
		}

		cout << res[(rows+cols-2)%4] << endl;
	}

	return 0;
}
