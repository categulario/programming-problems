import sys

if __name__ == '__main__':
	test_cases = int(sys.stdin.readline().strip())
	res = ('R', 'D', 'L', 'U')

	while test_cases > 0:
		rows, cols = list(map(int, sys.stdin.readline().strip().split(' ')))

		if rows > cols + 1:
			rows = cols + 1
		elif cols > rows:
			cols = rows

		print(res[(rows+cols-2)%4])

		test_cases-=1
