#include<stdio.h>
#include<math.h>

// store the tree
int arr[5050];
// the table of already computed answers
int ans[5050];
// for every answer, store wich of the childs is better (0 or 1)
short best_child[5050];

char letters[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

int first_index_of_level(int level) {
    return (level*(level+1))/2 - level;
}

int last_index_of_level(int level) {
    return (level*(level+1))/2 - 1;
}

int level_from_index(int index) {
    return floor(
          (1+sqrt(1+8*index)) /
        //-------------------
                   2
    );
}

int get_left_child_index(int index) {
    int level = level_from_index(index);
    int offset = index - first_index_of_level(level);

    return first_index_of_level(level + 1) + offset;
}

int get_right_child_index(int index) {
    int level = level_from_index(index);
    int offset = index - first_index_of_level(level);

    return first_index_of_level(level + 1) + offset + 1;
}

void print_tree(int levels) {
    for (int l = 1; l <= levels; l++) {
        int first_index = first_index_of_level(l);
        int last_index = last_index_of_level(l);

        for (int j = first_index; j <= last_index; j++) {
            printf("%d ", arr[j]);
        }

        printf("\n");
    }
}

void print_flat_tree(int levels) {
    int size = (levels*(levels+1))/2;

    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }

    printf("\n");
}

int max_cost(int index, int num_levels) {
    // check if already computed
    if (ans[index] != -1) {
        return ans[index];
    }

    int level = level_from_index(index);

    // check if last level, because there are no childs
    if (level == num_levels) {
        ans[index] = arr[index];
        return ans[index];
    }

    int left_child_index = get_left_child_index(index);
    int right_child_index = get_right_child_index(index);

    int left_max_cost = max_cost(left_child_index, num_levels);
    int right_max_cost = max_cost(right_child_index, num_levels);

    if (left_max_cost > right_max_cost) {
        // left path better than right
        ans[index] = arr[index] + left_max_cost;
        best_child[index] = 0;
    } else {
        // right path better than left
        ans[index] = arr[index] + right_max_cost;
        best_child[index] = 1;
    }

    return ans[index];
}

int get_weird_sum(int num_levels) {
    int index = 0;
    int level = 1;
    int sum = 0;

    while (level <= num_levels) {
        sum += arr[index]*arr[index];
        level += 1;

        if (best_child[index] == 0) {
            // best is left
            index = get_left_child_index(index);
        } else {
            // best is right
            index = get_right_child_index(index);
        }
    }

    return sum;
}

int main() {
    int n, j;

    scanf("%d", &n);

    int last_index = (n*(n+1))/2 - 1;

    for (int i = 0; i <= last_index; i++) {
        ans[i] = -1;
    }

    for (int i = 0; i <= last_index; i++) {
        scanf("%d", &j);
        arr[i] = j;
    }

    int sum = max_cost(0, n);
    int weird_sum = get_weird_sum(n);

    printf("%d %d\n", weird_sum, sum);
    printf("%c%c\n", letters[weird_sum%26], letters[sum%26]);

    // print_tree(3);
    // print_flat_tree(3);
}
