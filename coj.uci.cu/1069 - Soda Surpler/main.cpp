#include<iostream>

using namespace std;

int main() {
    int n;

    cin >> n;

    while (n-- > 0) {
        int e, f, c;

        cin >> e >> f >> c;

        if (c == 1) {
            cout << 0 << endl;
            continue;
        }

        int bottles = e + f;
        int num = 0;

        while (bottles >= c) {
            int bought = bottles/c;
            num += bought;
            bottles = bottles%c + bought;
        }

        cout << num << endl;
    }
}
