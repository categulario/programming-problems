#include <stdio.h>
#include <math.h>

long int max(long int a, long int b) {
    return a > b ? a : b;
}

void solve(long int low, long int high) {
    int results = 0;

    for (long int i = max(low, 4); i <= high; i++) {
        int divisor = 0;
        int times = 0;
        long int left = i;
        bool useful = true;

        for (int j = 2; j <= (i/2+1) && left != 1; j++) {
            if (left%j == 0) { // j divides i
                if (divisor == 0) {
                    divisor = j;
                } else if (divisor != j) { // a different divisor, abort
                    useful = false;
                    break;
                }

                while (left%j == 0) {
                    left /= j;

                    times += 1;
                }
            }
        }

        if (useful && times > 1) {
            results += 1;
        }
    }

    printf("%d\n", results);
}

int main() {
    int n;
    long int low, high;

    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        scanf("%ld %ld", &low, &high);
        solve(low, high);
    }
}
