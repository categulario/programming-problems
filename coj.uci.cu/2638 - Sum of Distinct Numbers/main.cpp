#include <iostream>

using namespace std;

int tree(int n, int starting_with) {
	int sum = 0;

	if (starting_with>n) {
		return 0;
	} else if (starting_with == n) {
		return 1;
	}

	for (int i = starting_with+1; i <= n - starting_with; i++) {
		// if (i == n - starting_with)
		// 	sum += 1;
		// else if (i < n - starting_with)
			sum += tree(n - starting_with, i);
	}

	return sum%100999;
}

int solution(int n) {
	int sum = 0;
	for (int i = 1; i <= n; i++) {
		sum += tree(n, i);
	}
	return sum%100999;
}

int main() {
	int cases, n;
	cin >> cases;

	while (cases--) {
		cin >> n;
		cout << solution(n) << endl;
	}

	return 0;
}
