import sys
from functools import reduce

while True:
    line = sys.stdin.readline().strip()

    if line == '*':
        break

    ans = reduce(
        lambda x, y: y if x == y else False,
        map(
            lambda x: x.lower(),
            map(
                lambda x: x[0],
                line.split()
            )
        ),
    )

    print('N' if not ans else 'Y')
