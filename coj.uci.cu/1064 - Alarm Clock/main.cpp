#include <iostream>

using namespace std;

int main() {
	int h1, m1, h2, m2, res;

	while (true) {
		cin >> h1 >> m1 >> h2 >> m2;
		if (h1==m1 and m1==h2 and h2==m2 and m2==0) {
			break;
		}
		res = (h2-h1)*60 + (m2-m1);
		if (res < 0) {
			res += 1440;
		}
		cout << res << endl;
	}
}
