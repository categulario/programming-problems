#include <string>
#include <iostream>

using namespace std;

bool checked_states[1024];

int encode(string line) {
    int n = 0;
    int num_words = 0;
    bool is_first_of_word = true;

    for (int i = 0; i<line.length() && num_words < 5; i++) {
        if (is_first_of_word) {
            n <<= 2;

            switch (line[i]) {
                case 'H':
                    n += 0;
                    break;
                case 'S':
                    n += 1;
                    break;
                case 'D':
                    n += 2;
                    break;
                case 'C':
                    n += 3;
                    break;
            }

            is_first_of_word = false;
            num_words += 1;
        }

        if (line[i] == ' ') {
            is_first_of_word = true;
        }
    }

    return n;
}

int move_wheel(int wheel_no, int state) {
    // will hold the state of each wheel
    int pieces[5];
    int tmp = state;

    // move the wheel states to an array
    for (int i = 0; i < 5; i++) {
        pieces[4-i] = tmp%4;
        tmp >>= 2;
    }

    if (wheel_no == 0) {
        pieces[0] = (pieces[0]+1)%4;
        pieces[1] = (pieces[1]+1)%4;
        pieces[2] = (pieces[2]+1)%4;
    } else if (wheel_no == 1) {
        pieces[1] = (pieces[1]+1)%4;
        pieces[0] = (pieces[0]+1)%4;
        pieces[4] = (pieces[4]+1)%4;
    } else if (wheel_no == 2) {
        pieces[2] = (pieces[2]+1)%4;
        pieces[1] = (pieces[1]+1)%4;
        pieces[3] = (pieces[3]+1)%4;
    } else if (wheel_no == 3) {
        pieces[3] = (pieces[3]+1)%4;
        pieces[0] = (pieces[0]+1)%4;
        pieces[4] = (pieces[4]+1)%4;
    } else if (wheel_no == 4) {
        pieces[4] = (pieces[4]+1)%4;
        pieces[2] = (pieces[2]+1)%4;
        pieces[3] = (pieces[3]+1)%4;
    }

    tmp = 0;

    for (int i = 0; i < 5; i++) {
        tmp <<= 2;
        tmp += pieces[i];
    }

    return tmp;
}

bool can_be_generated(int goal, int current_state) {
    // Found it! stop recursion immediatly
    if (goal == current_state) {
        return true;
    }

    // this state was already generated, don't continue
    if (checked_states[current_state]) {
        return false;
    }

    // store that we just checked this state so it is never checked again
    checked_states[current_state] = true;

    // move each of the cylinders to genereate a new combination
    for (int w=0; w<5; w++) {
        int new_state = move_wheel(w, current_state);

        if (can_be_generated(goal, new_state)) {
            return true;
        }
    }

    return false;
}

int main() {
    string line;
    int initial_state;
    int goal;

    while (true) {
        getline(cin, line);

        if (line.length() == 0) {
            break;
        }

        for (int i = 0; i<1024; i++) {
            checked_states[i] = false;
        }

        initial_state = encode(line);

        getline(cin, line);

        goal = encode(line);

        if (can_be_generated(goal, initial_state)) {
            cout << "The necklace can be useful." << endl;
        } else {
            cout << "The necklace can not be useful." << endl;
        }
    }
}
