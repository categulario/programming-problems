#include<stdio.h>

int main() {
    unsigned int n, weight;
    int cur;
    bool has_printed;

    scanf("%d", &n);

    while (n--) {
        scanf("%d", &weight);
        cur = 0;
        has_printed = false;

        while (weight > 0) {
            if (weight %2 == 1) {
                if (has_printed) {
                    printf(" %d", cur);
                } else {
                    printf("%d", cur);
                    has_printed = true;
                }
            }

            weight /= 2;
            cur++;
        }

        printf("\n");
    }
}
