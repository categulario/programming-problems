#include<string>
#include<iostream>
#include<stdio.h>

using namespace std;

int main() {
    int n;
    string s;
    char last;

    scanf("%d\n", &n);

    while (n--) {
        getline(cin, s);

        last = s[s.length()-1];

        if (last%2 == 0) {
            printf("even\n");
        } else {
            printf("odd\n");
        }

        s.clear();
    }
}
