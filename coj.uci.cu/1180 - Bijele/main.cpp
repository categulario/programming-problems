#include <iostream>

using namespace std;

int main() {
	int cases, kings, queens, rooks, bishops, knights, pawns;
	int kings_expected = 1;
	int queens_expected = 1;
	int rooks_expected = 2;
	int bishops_expected = 2;
	int knights_expected = 2;
	int pawns_expected = 8;
	cin >> cases;

	while(cases--) {
		cin >> kings >> queens >> rooks >> bishops >> knights >> pawns;

		cout << kings_expected - kings << " ";
		cout << queens_expected - queens << " ";
		cout << rooks_expected - rooks << " ";
		cout << bishops_expected - bishops << " ";
		cout << knights_expected - knights << " ";
		cout << pawns_expected - pawns << endl;
	}
}
