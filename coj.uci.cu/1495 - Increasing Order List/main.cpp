#include <iostream>
#include <utility>

using namespace std;

void build_tree(int *heap, int last_pos) {
	while (last_pos>0) {
		int parent = (last_pos-1)/2;
		if (heap[last_pos] > heap[parent]) {
			int aux        = heap[last_pos];
			heap[last_pos] = heap[parent];
			heap[parent]   = aux;

			last_pos = parent;
		} else {
			break;
		}
	}
}

void print_arr(int *arr, int len) {
	for (int i=0; i<len; i++) {
		cout << arr[i] << endl;
	}
}

void fix_heap(int *heap, int last_pos) {
	int cur_pos = 0; // position of disordered item
	while (cur_pos < last_pos) {
		int child1 = cur_pos*2 + 1, child2 = cur_pos*2+2;

		if (child2 <= last_pos) {
			// the tree is a heap
			if (heap[cur_pos] > heap[child1] && heap[cur_pos] > heap[child2]) {
				break;
			}

			if (heap[child1] > heap[child2]) {
				if (heap[cur_pos] < heap[child1]) {
					swap(heap[child1], heap[cur_pos]);
					cur_pos = child1;
				} else if (heap[cur_pos] < heap[child2]) {
					swap(heap[child2], heap[cur_pos]);
					cur_pos = child2;
				} else {
					break;
				}
			} else if (heap[cur_pos] < heap[child2]) {
				swap(heap[child2], heap[cur_pos]);
				cur_pos = child2;
			} else {
				break;
			}
		} else if (child1 <= last_pos) {
			if (heap[cur_pos] > heap[child1]) {
				break;
			} else {
				swap(heap[child1], heap[cur_pos]);
				cur_pos = child1;
			}
		} else {
			break;
		}
	}
}

int main () {
	int items, last_pos;

	cin >> items;

	int heap[items];

	// input elements and build the heap
	for (int i=0; i<items; i++) {
		cin >> heap[i];
		build_tree(heap, i);
	}

	// sort the array
	last_pos = items-1;
	while (last_pos > 0) {
		swap(heap[0], heap[last_pos]);
		last_pos--;
		fix_heap(heap, last_pos);
	}

	print_arr(heap, items);

	return 0;
}
