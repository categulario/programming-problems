# Fear to Mines

## Description

Do you have fear of the mines? Well, Billy also fears them, but do not worry. This is just a game, and there is no possibility of danger to you. Mine Game, is a board of R rows and C columns (1 <= R <= 20; 1 <= C <= 20) which are placed D (0 <= D <= R*C) land mines. The information is processed, and for each square on the board, except for those with a mine, generates a number corresponding to the number of mines in the 8 or less squares adjacent to it. At the beginning of the game, you are given a configuration of the visibility of each square on the board, and the numbers of mines. Of course, you also know information on each of the squares visible, but the rest is unknown during the game. However, if you are a good player of Mines, and dominates a little math come to recognize the entire board without crashing into a mine. Of course, if you chash some mine, you lose the game in question immediately. There will be no visible mines at the beginning. Billy, with his 10 years of age have to much fear of mines. And you, decide to develop a program that tell him, with all the information provided, if possible, or not winning the game without the risk at some point crash a mine. Billy is very good with the basic math, but he do not want to play with fate at any moment, their fate has never been very good and he is sure that he choose the wrong option, and then he crash a mine. Please, Help him!!! Billy, can not do any complicated logical deduction, has just 10 years of age, so it only takes into account the information of the visible square that he is analyzing. See example input.

## Input spec

The input consists of several test cases. You must read until you reach the end of file. Each test case starts with three integers: R, C, and M. Then follow M lines, each with two integers corresponding to the row and column of a mine on the board. Finally, a binary matrix of R rows and C columns, with the configuration of initial visibility of the board. Ones means that this squares are visibles, and of course, zeros means that it is not initially visible. There will be a blank line of separation between each case. Not will be line after the last. There will be no two mines with the same positions.

## Output spec

A single line, with a word. If Billy can win the game without the risk at some point crash a mine write "YES". Write "NO" otherwise.

## Sample input

```
3 3 2
1 3
2 1
0 1 0
0 1 1
0 0 1

2 2 2
1 2
2 1
1 0
0 1
```

## Sample output

```
NO
YES
```
