#include <iostream>

using namespace std;

int main() {
	int test_cases, kids, candies=0, tmp;
	cin >> test_cases;

	while (test_cases-- > 0) {
		cin >> kids;
		for (int i = 0; i < kids; i++) {
			cin >> tmp;

			candies += tmp;
		}

		if (candies%kids == 0) {
			cout << "YES" << endl;
		} else {
			cout << "NO" << endl;
		}

		candies = 0;
	}

	return 0;
}
