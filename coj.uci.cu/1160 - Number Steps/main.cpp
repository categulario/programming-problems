#include <iostream>

using namespace std;

int main() {
	int test_cases, x, y;

	cin >> test_cases;

	while (test_cases--) {
		cin >> x >> y;
		if (x == y) {
			if (x%2 == 0) {
				cout << 2*x << endl;
			} else {
				cout << 2*x-1 << endl;
			}
		} else if (x == y+2) {
			if (x%2==0) {
				cout << x*2-2 << endl;
			} else {
				cout << x*2-3 << endl;
			}
		} else {
			cout << "No Number" << endl;
		}
	}
}
