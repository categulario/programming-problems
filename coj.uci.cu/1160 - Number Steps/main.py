import sys

if __name__ == '__main__':
	test_cases = int(sys.stdin.readline().strip())

	while test_cases:
		x, y = list(map(int, sys.stdin.readline().strip().split(' ')))
		if x == y:
			if x%2 == 0:
				print(2*x)
			else:
				print(2*x-1)
		elif x == y+2:
			if x%2==0:
				print(x*2-2)
			else:
				print(x*2-3)
		else:
			print("No Number")

		test_cases -= 1
