#include<stdio.h>

int main() {
    int n, b, t, cuanto;
    char tipo;

    scanf("%d", &n);

    while (n--) {
        scanf("%d\n%d\n", &b, &t);

        while (t--) {
            scanf("%c %d\n", &tipo, &cuanto);

            if (tipo == 'C') {
                b += cuanto;
            } else if (tipo == 'D') {
                b -= cuanto;
            }
        }

        printf("%d\n", b);
    }
}
