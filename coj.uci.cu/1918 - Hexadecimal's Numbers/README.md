#  1918 - Hexadecimal's Numbers 

## Description

One beautiful July morning a terrible thing happened in Mainframe: a mean virus Megabyte somehow got access to the memory of his not less mean sister Hexadecimal. He loaded there a huge amount of n different natural numbers from 1 to n to obtain total control over her energy.

But his plan failed. The reason for this was very simple: Hexadecimal didn't perceive any information, apart from numbers written in binary format. This means that if a number in a decimal representation contained characters apart from 0 and 1, it was not stored in the memory. Now Megabyte wants to know, how many numbers were loaded successfully.

## Input spec

The first line of input contains an integer T (1 <= T <= 40), the number of test cases. The following T lines contain an integer N (1 <= N <= 10^9).

## Output spec

The output will contain T lines with the answer for each test case.

## Sample input

```
1
10
```

## Sample output

```
2
```
