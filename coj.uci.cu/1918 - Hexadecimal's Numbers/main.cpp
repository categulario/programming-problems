#include <iostream>
#include <math.h>

using namespace std;

int binaries_count(int num) {
	if (num == 0) {
		return 0;
	}
	int digits = floor(log10(num));

	int power = int(pow(10, digits));

	if (num-power >= power)
		return pow(2, digits+1)-1;
	else
		return pow(2, digits) + binaries_count(num%power);
}

int main() {
	int cases=1000000000, n;
	cin >> cases;

	while (cases--) {
		cin >> n;
		cout << binaries_count(n) << endl;
	}
}
