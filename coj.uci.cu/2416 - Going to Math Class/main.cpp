#include <stdio.h>

int digit_sum(int n) {
    int sum = 0;

    for (int i = 0; i < 6; i++) {
        sum += n%10;
        n /= 10;
    }

    return sum;
}

int solve(int n) {
    for (int d = 1; d <= 9; d++) {
        int six_dig = n*10 + d;

        for (int sum = 1; sum <= 9*6; sum ++) {
            int maybe_num = six_dig + sum;

            if (digit_sum(maybe_num) == sum) {
                return (maybe_num - sum)%10;
            }
        }
    }

    return 0;
}

int main() {
    int t, n;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        scanf("%d", &n);

        printf("%d\n", solve(n));
    }
}
