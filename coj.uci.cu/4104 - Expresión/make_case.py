from random import choice

opts = ['X++;', 'X--;', '--X;', '++X;']

print(''.join(choice(opts) for _ in range(250)))
