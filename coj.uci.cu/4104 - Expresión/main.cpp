#include<string>
#include<iostream>

using namespace std;

int main() {
    string s;
    int c=0, i=0;
    bool is_pre = false;
    int jump = 3;

    cin >> s;

    while (i<s.length()) {
        if (s[i] == 'X') {
            i+=1;
            jump = 2;
        } else if (s[i] == '-') {
            c--;
            i += jump;
            jump = 3;
        } else if (s[i] == '+') {
            c++;
            i += jump;
            jump = 3;
        } else {
            i++;
        }
    }

    cout << c << endl;
}
