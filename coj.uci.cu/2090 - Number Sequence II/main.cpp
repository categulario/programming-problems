#include <stdio.h>
#include <math.h>

int digits_of_sn(int n) {
    int k = floor(log10(n)) + 1;
    int sum = 0;

    for (int i = 0; i < k; i++) {
        sum += n - (pow(10, i)-1);
    }

    return sum;
}

int digits_of_n(int n) {
    return floor(log10(n)) + 1;
}

int nth_digit_of(int n, int i) {
    return ((int) (n/pow(10, digits_of_n(n)-i)))%10;
}

void solution(int i) {
    int prev_s = 0;
    int cur_s = 1;
    int digit_count = 0;

    while (digit_count < i) {
        digit_count += digits_of_sn(cur_s);
        prev_s ++;
        cur_s  ++;
    }

    digit_count -= digits_of_sn(cur_s-1);

    int prev_num = 0;
    int cur_num = 1;

    while (digit_count < i) {
        digit_count += digits_of_n(cur_num);
        prev_num ++;
        cur_num ++;
    }

    digit_count -= digits_of_n(cur_num-1);

    int n = cur_num-1;
    int d = nth_digit_of(n, i-digit_count);

    printf("%d %d %d\n", d, n, prev_s);
}

int main() {
    int t, i;

    scanf("%d", &t);

    for (int j=0; j<t; j++) {
        scanf("%d", &i);

        solution(i);
    }
}
