#include<stdio.h>

int main() {
    int M, T, U, F, D;
    char letra;

    scanf("%d %d %d %d %d", &M, &T, &U, &F, &D);

    U += D;
    F *= 2;

    int cont = 0;
    int sum = 0;
    int val;

    for (int i=0; i<T; i++) {
        getchar();
        letra = getchar();

        if (letra == 'U' || letra == 'D') {
            val = U;
        } else if (letra == 'F') {
            val = F;
        } else {
            printf("Error: %c\n", letra);
        }

        if (sum + val > M) {
            break;
        }

        sum += val;
        cont += 1;
    }

    printf("%d\n", cont);
}
