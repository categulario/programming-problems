#include<stdio.h>

//     María
//       v
int grid[19][19];
//            ^
//          Nieves

// Just prints the grid
void print_grid(int marias, int nieves) {
    for (int i = 0; i < marias; i++) {
        for (int j = 0; j < nieves; j++) {
            printf("%d ", grid[i][j]);
        }
        printf("\n");
    }
}

int main() {
    // set zeros in the matrix
    for (int i = 0; i < 19; i++) {
        for (int j = 0; j < 19; j++) {
            grid[i][j] = 0;
        }
    }

    // start the input
    int marias, nieves;

    scanf("%d %d", &marias, &nieves);

    // enter marias friends
    for (int i = 0; i < marias; i++) {
        int numfriends, f;

        scanf("%d", &numfriends);

        for (int j = 0; j < numfriends; j++) {
            scanf("%d", &f);

            grid[i][f-1] += 1;
        }
    }

    // enter nieves friends
    for (int i = 0; i < nieves; i++) {
        int numfriends, f;

        scanf("%d", &numfriends);

        for (int j = 0; j < numfriends; j++) {
            scanf("%d", &f);

            grid[f-1][i] += 1;
        }
    }
}
