#include <stdio.h>
#include <stdlib.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

char word[1000000];
char tmp[1000000];

void print_word(int word_length) {
    for (int i = 0; i < word_length; i++) {
        printf("%c", tmp[i]);
    }

    printf("\n");
}

/* Determines if the word is balanced given the rules of the problem */
bool is_balanced(int size, int start, int end) {
    int l_count = 0, p_count = 0;
    char group = 'A';
    int group_count = 0;
    int j = MIN(size-1, end);

    while (j >= MAX(0, start)) {
        char res = tmp[j];

        if (res == 'L') {
            l_count += 1;
        } else {
            p_count += 1;
        }

        if (group == res) {
            group_count += 1;
        } else {
            group = res;
            group_count = 1;
        }

        if (abs(l_count - p_count) > 2) {
            return false;
        }
        if (group_count > 2) {
            return false;
        }

        j -= 1;
    }

    return true;
}

bool is_above(int depth) {
    for (int i = 0; i<=depth; i++) {
        if (tmp[i] > word[i]) {
            return false;
        }

        if (tmp[i] != word[i]) {
            break;
        }
    }

    return true;
}

/* count how many balanced strings are there before the given one */
int words_above(int word_length, int modulo, int depth, char try_letter) {
    tmp[depth] = try_letter;

    if (!is_balanced(word_length, 0, depth)) {
        return 0;
    }

    if (!is_above(depth)) {
        return 0;
    }

    if (depth == word_length - 1) {
        // print_word(word_length);

        return 1;
    }

    return (
        words_above(word_length, modulo, depth+1, 'L') +
        words_above(word_length, modulo, depth+1, 'P')
    ) % modulo;
}

int main() {
    // getting the data
    int word_length, modulo;

    scanf("%d\n", &word_length);
    scanf("%d\n", &modulo);

    for (int i=0; i<word_length; i++) {
        word[i] = getchar();
    }

    // solving the problem
    int solution = (
        words_above(word_length, modulo, 0, 'L') +
        words_above(word_length, modulo, 0, 'P')
    ) % modulo;

    printf("%d\n", solution);
}
