def is_balanced(num, size):
    count = [0, 0]
    group = None
    group_count = 0

    while size > 0:
        res = num % 2
        count[res] += 1

        if group == res:
            group_count += 1
        else:
            group = res
            group_count = 1

        # print(count)
        if abs(count[0] - count[1]) > 2:
            return False
        if group_count > 2:
            return False

        size -= 1
        num = num >> 1

    return True


def how_many(digits):
    start = 2**digits - 1
    count = 0

    while start >= 0:
        if is_balanced(start, digits):
            count += 1
            print("{:0>2d} {:0>5b}".format(count, start))

        start -= 1

    return count


def main():
    # for i in range(1, 10):
        # print(i, how_many(i))
    how_many(5)


def test():
    # assert is_balanced(0b00101, 5)
    # assert not is_balanced(0b0, 5)
    # assert not is_balanced(0b11100, 5)
    # assert not is_balanced(0b111100, 6)
    # assert not is_balanced(0b000011, 6)
    assert not is_balanced(0b001001, 6)


if __name__ == '__main__':
    main()
