#!/usr/bin/env python3
import os


def main():
    code = input('Código del problema: ')
    name = input('Nombre del problema: ')

    pathname = os.path.join('codeforces.com', code, name)

    os.makedirs(pathname, exist_ok=True)

    print('path created: "{}"'.format(pathname))

    files = ('in.txt', 'out.txt', 'main.rs')

    for filename in files:
        with open(os.path.join(pathname, filename), 'w') as f:
            if filename == 'main.rs':
                f.write("""use std::fmt;
use std::str::FromStr;
use std::io;

fn main() {
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read<T: FromStr>() -> T
where <T as FromStr>::Err: fmt::Debug
{
    read_line().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
where <T as FromStr>::Err: fmt::Debug
{
    read_line().split(" ").map(|x| x.parse().unwrap()).collect()
}
""")

if __name__ == '__main__':
    main()
