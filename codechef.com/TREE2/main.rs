use std::fmt;
use std::str::FromStr;
use std::io;

struct Node {
    value: u64,
    left: Option<Box<Node>>,
    right: Option<Box<Node>>,
}

impl Node {
    fn new(value: u64) -> Node {
        Node {
            value,
            left: None,
            right: None,
        }
    }

    fn add(&mut self, value: u64) {
        if value == self.value {
            return;
        }

        if value > self.value {  // vamos a la derecha
            match &mut self.right {
                Some(ref mut node) => {
                    node.add(value);
                }
                None => {
                    self.right = Some(Box::new(Node::new(value)));
                }
            }
        } else {  // vamos a la izquierda
            match &mut self.left {
                Some(ref mut node) => {
                    node.add(value);
                }
                None => {
                    self.left = Some(Box::new(Node::new(value)));
                }
            }
        }
    }

    fn size(&self) -> usize {
        self.left.as_ref().map(|n| n.size()).unwrap_or(0) +
        self.right.as_ref().map(|n| n.size()).unwrap_or(0) +
        1
    }
}

fn case(sticks: Vec<u64>) -> usize {
    let mut iter = sticks.iter();

    let mut tree = Node::new(*iter.next().unwrap());

    for value in iter {
        tree.add(*value);
    }

    tree.size()
}

fn main() {
    let t: u32 = read();

    for _ in 0..t {
        let _n: u64 = read();
        let sticks: Vec<u64> = read_vec();

        println!("{}", case(sticks));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
