use std::fmt;
use std::str::FromStr;
use std::io;
use std::collections::HashMap;

fn how_many_paths(n: u64, k: u64, d: u64, has_d: bool, curr_sum: u64, cache: &mut HashMap<(u64, bool), u64>) -> u64 {
    if let Some(&r) = cache.get(&(curr_sum, has_d)) {
        return r;
    }

    if curr_sum == n {
        return if has_d { 1 } else { 0 };
    } else if curr_sum > n {
        return 0;
    }

    // seguir bajando
    let mut sum = 0;

    for i in 1..=k {
        sum += how_many_paths(n, k, d, has_d || i >= d, curr_sum + i, cache);
    }

    sum = sum % 1000000007;

    cache.insert((curr_sum, has_d), sum);

    sum
}

fn solve(n: u64, k: u64, d: u64) -> u64 {
    let mut cache = HashMap::new();

    how_many_paths(n, k, d, false, 0, &mut cache)
}

fn main() {
    let nkd: Vec<u64> = read_vec();

    let n = nkd[0];
    let k = nkd[1];
    let d = nkd[2];

    println!("{}", solve(n, k, d));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
