use std::fmt;
use std::str::FromStr;
use std::io;

fn solucion(acuario: &[u32]) -> i32 {
    let mut son_todos_iguales = true;
    let mut max = acuario[0];
    let mut max_index: i32 = 0;
    let mut max_end_range: i32 = 0;

    let mut rest = acuario.iter().enumerate();

    rest.next(); // saltarse el primer elemento

    for (i, p) in rest {
        if *p > max {
            max = * p;
            max_index = i as i32;
            max_end_range = i as i32;
            son_todos_iguales = false;
        } else if *p < max {
            son_todos_iguales = false;

            if i as i32 == max_end_range + 1 {
                max_index = max_end_range;
            }
        } else { // la piraña actual es igual que el máximo
            if i as i32 == max_end_range + 1 {
                max_end_range += 1;
            }
        }
    }

    if son_todos_iguales {
        -1
    } else {
        max_index + 1
    }
}

fn main() {
    let t: u32 = read();

    for _ in 0..t {
        let _n: u32 = read();
        let acuario: Vec<u32> = read_vec();

        println!("{}", solucion(&acuario));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
