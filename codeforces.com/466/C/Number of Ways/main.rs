use std::fmt;
use std::str::FromStr;
use std::io;

fn solve(nums: &[i64]) -> u64 {
    let sum: i64 = nums.iter().sum();

    if sum % 3 != 0 {
        return 0;
    }

    let third = sum / 3;

    let (_, mut rev): (_, Vec<u64>) = nums
        .iter()
        .rev()
        .fold((0, Vec::with_capacity(nums.len())), |(sum, mut acc), &e| {
            let sum = sum + e;

            if let Some(&last) = acc.last() {
                if sum == third {
                    acc.push(last + 1);
                } else {
                    acc.push(last);
                }
            } else {
                // estoy en el primer elemento del nuevo vector
                if sum == third {
                    acc.push(1);
                } else {
                    acc.push(0);
                }
            }

            (sum, acc)
        });

    rev.reverse();

    let mut rev: Vec<_> = rev.into_iter().skip(2).collect();

    rev.push(0);
    rev.push(0);

    let (_, ans) = nums
        .iter() // <- e
        .zip(rev.iter()) // <- r
        .fold((0, 0), |(sum, ans), (e, r)| {
            let sum = sum + e;

            (sum, if sum == third {
                ans + r
            } else {
                ans
            })
        });

    ans
}

fn main() {
    let _n: u64 = read();
    let nums: Vec<i64> = read_vec();

    println!("{}", solve(&nums));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
