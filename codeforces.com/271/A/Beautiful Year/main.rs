use std::fmt;
use std::str::FromStr;
use std::io;

fn tiene_cifras_diferentes(mut n: u16) -> bool {
    let mut arr = [false; 10];

    while n != 0 {
        let d = n % 10;

        if arr[d as usize] {
            return false;
        }

        arr[d as usize] = true;
        n /= 10;
    }

    true
}

fn solve(mut n: u16) -> u16 {
    n += 1;

    loop {
        if tiene_cifras_diferentes(n) {
            return n;
        }

        n += 1;
    }
}

fn main() {
    let n: u16 = read();

    println!("{}", solve(n));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read<T: FromStr>() -> T
where <T as FromStr>::Err: fmt::Debug
{
    read_line().parse().unwrap()
}
