use std::fmt;
use std::str::FromStr;
use std::io;
use std::collections::HashMap;

struct Resultados {
    longitud_propia: u32,
    longitud_maxima: u32,
}

#[derive(Debug)]
struct Spider {
    data: HashMap<usize, Vec<usize>>,
}

impl Spider {
    fn new() -> Spider {
        Spider {
            data: HashMap::new(),
        }
    }

    fn connect(&mut self, c1: usize, c2: usize) {
        let lista = self.data.entry(c1).or_insert(Vec::new());
        lista.push(c2);

        let lista = self.data.entry(c2).or_insert(Vec::new());
        lista.push(c1);
    }
}

fn mide_pata(s: &Spider, desde: usize, hacia: usize) -> Resultados {
    let mut medidas = Vec::new();
    let mut longitud_maxima = 0;

    for hijo in s.data.get(&hacia).unwrap().iter().filter(|h| *h != &desde) {
        let Resultados {
            longitud_propia: lp,
            longitud_maxima: lm,
        } = mide_pata(s, hacia, *hijo);
        medidas.push(lp);

        if lm > longitud_maxima {
            longitud_maxima = lp;
        }
    }

    let sublongitud_maxima = if medidas.len() == 0 {
        1
    } else if medidas.len() == 1 {
        medidas[0]
    } else {
        medidas.sort_unstable_by(|a, b| b.cmp(a));

        medidas[0] + medidas[1]
    };

    Resultados {
        longitud_propia: medidas.iter().max().unwrap_or(&0) + 1,
        longitud_maxima: if sublongitud_maxima > longitud_maxima { sublongitud_maxima } else { longitud_maxima },
    }
}

fn mide(s: Spider) -> u32 {
    let bola_inicial = s.data.keys().next().unwrap();
    let mut resultados = Vec::new();

    // println!("Araña empezando en {}", bola_inicial);

    for hijo in s.data.get(bola_inicial).unwrap().iter() {
        let Resultados{ longitud_propia: suma, longitud_maxima: longitud_maxima} = mide_pata(&s, *bola_inicial, *hijo);
        resultados.push(suma);

        // println!("hijo {} suma {}", hijo, suma);
    }

    if resultados.len() == 1 {
        resultados[0]
    } else {
        resultados.sort_unstable_by(|a, b| b.cmp(a));

        resultados[0] + resultados[1]
    }
}

fn main() {
    let n: u32 = read();
    let mut suma = 0;

    for _i in 0..n {
        let spider: Vec<usize> = read_vec();
        let mut s = Spider::new();

        for j in 0..(spider[0] - 1) {
            let c1 = spider[j*2 + 1];
            let c2 = spider[j*2 + 2];

            s.connect(c1, c2);
        }

        suma += mide(s);
    }

    println!("{}", suma);
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
