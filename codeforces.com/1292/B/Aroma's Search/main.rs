use std::fmt;
use std::str::FromStr;
use std::io;

#[derive(Copy, Clone, Debug)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }

    fn next(self, a_x: i64, a_y: i64, b_x: i64, b_y: i64) -> Option<Point> {
        Some(Point {
            x: a_x.checked_mul(self.x)?.checked_add(b_x)?,
            y: a_y.checked_mul(self.y)?.checked_add(b_y)?,
        })
    }

    fn distance(&self, other: Point) -> i64 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

fn solve(x_0: i64, y_0: i64, a_x: i64, a_y: i64, b_x: i64, b_y: i64, x_s: i64, y_s: i64, t: i64) -> i64 {
    let mut points = Vec::new();
    let mut count = 0;
    let mut prev = Point::new(x_0, y_0);
    let mut alma = Point::new(x_s, y_s);

    points.push(prev);

    loop {
        if let Some(p) = prev.next(a_x, a_y, b_x, b_y) {
            points.push(p);
            prev = p;
        } else {
            break;
        }
    }

    dbg!(&points);

    let (index, closest) = points.iter().enumerate().min_by(|(_i1, p1), (_i2, p2)| {
        p1.distance(alma).cmp(&p2.distance(alma))
    }).unwrap();

    dbg!(index);
    dbg!(closest);

    let mut i = index;
    let mut cur = *closest;
    let mut consumed_time = 0;

    while consumed_time + dbg!(cur.distance(alma)) <= t {
        consumed_time += cur.distance(alma);
        alma = cur;
        count += 1;

        if i == 0 {
            break;
        }

        i -= 1;
        cur = points[i];
    }

    if let Some(p) = points.get(index + 1) {
        cur = *p;

        while consumed_time + dbg!(dbg!(cur).distance(alma)) <= t {
            consumed_time += cur.distance(alma);
            alma = cur;
            count += 1;

            if let Some(p) = points.get(i + 1) {
                cur = *p;
                i += 1;
            } else {
                break;
            }
        }
    }

    count
}

fn main() {
    let first_line: Vec<i64> = read_vec();
    let second_line: Vec<i64> = read_vec();

    if let [x_0, y_0, a_x, a_y, b_x, b_y] = &first_line[..] {
        if let [x_s, y_s, t] = &second_line[..] {
            println!("{}", solve(*x_0, *y_0, *a_x, *a_y, *b_x, *b_y, *x_s, *y_s, *t));
        }
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
