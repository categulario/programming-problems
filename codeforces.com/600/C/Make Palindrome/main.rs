use std::io;
use std::collections::HashMap;

fn solve(s: String) -> String {
    // hacemos el conteo de letras
    let cuentas = {
        let mut cuentas = HashMap::new();

        for c in s.chars() {
            let cont = cuentas.entry(c).or_insert(0);
            *cont += 1;
        }

        cuentas
    };

    let (mut pares, huerfanito) = {
        let mut impares = Vec::new();
        let mut pares = Vec::new();

        for (c, cuantos) in cuentas.into_iter() {
            if cuantos % 2 == 0 {
                pares.push((c, cuantos));
            } else {
                impares.push(c);

                if cuantos > 2 {
                    pares.push((c, cuantos - 1));
                }
            }
        }

        impares.sort_unstable();

        pares.extend(impares.iter().take(impares.len() / 2).map(|c| (*c, 2)));

        pares.sort_unstable_by_key(|p| p.0);

        (pares, if impares.len() % 2 != 0 {
            Some(impares[impares.len() / 2])
        } else { None })
    };

    let mut resultado = String::with_capacity(s.len());

    for (c, cuantos) in pares.iter_mut() {
        for _ in 0..(*cuantos / 2) {
            resultado.push(*c);
        }

        *cuantos /= 2;
    }

    // si hay una cantidad impar de impares poner el elemento que va enmedio de
    // la cadena
    if let Some(c) = huerfanito {
        resultado.push(c);
    }

    // los pares que faltan
    for (c, cuantos) in pares.into_iter().rev() {
        for _ in 0..cuantos {
            resultado.push(c);
        }
    }

    resultado
}

fn main() {
    let s = read_line();

    println!("{}", solve(s));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}
