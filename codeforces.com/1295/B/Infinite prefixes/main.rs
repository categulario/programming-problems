use std::str::FromStr;
use std::fmt;
use std::io;

enum Ans {
    Inf,
    Num(u64),
}

fn caso(cadena: &str, k: u64) -> Ans {
    let mut balances = Vec::with_capacity(cadena.len());
    let mut cur_balance: u64 = 0;

    for c in cadena.chars() {
        if c == '0' {
            cur_balance += 1;
        } else {
            cur_balance -= 1;
        }

        balances.push(cur_balance);
    }

    let b = *balances.last().unwrap();

    if b == 0 {
        if balances.contains(&k) {
            return Ans::Inf;
        } else if k == 0 {
            return Ans::Num(1);
        } else {
            return Ans::Num(0);
        }
    } else {
        let mut count: u64 = 0;

        if k == 0 {
            count += 1;
        }

        // m*b + a_i = k
        // m = (k - a_i) / b

        count += balances
            .iter()
            .filter(|a_i| {
                let aux = k - *a_i;

                if aux % b != 0 {
                    return false;
                }

                return aux / b >= 0;
            })
            .count() as u64;

        return Ans::Num(count);
    }
}

fn main() {
    let num_cases: u64 = read();

    for i in 0..num_cases {
        let n_k: Vec<u64> = read_vec();
        let cadena = read_line();

        let ans = caso(cadena.trim(), n_k[1]);

        match ans {
            Ans::Inf => println!("-1"),
            Ans::Num(x) => println!("{}", x),
        }
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
