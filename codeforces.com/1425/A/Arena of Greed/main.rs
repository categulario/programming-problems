use std::fmt;
use std::str::FromStr;
use std::io;

fn turno(n: &mut u64) -> u64 {
    if *n == 4 {
        *n = 2;
        2
    } else if *n == 0 {
        0
    } else if *n % 2 == 0 && *n % 4 != 0 {
        let r = *n / 2;

        *n = *n/2;

        r
    } else {
        *n -= 1;

        1
    }
}

fn solve(mut n: u64) -> u64 {
    let mut res = 0;

    while n > 0 {
        // el turno de Mr. Chanek
        res += turno(&mut n);

        // el turno de su oponente
        turno(&mut n);
    }

    res
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        println!("{}", solve(read()));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}
