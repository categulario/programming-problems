use std::fmt;
use std::str::FromStr;
use std::io;

enum Asociados {
    Uno((usize, usize)),
    Dos((usize, usize), (usize, usize)),
    Cuatro((usize, usize), (usize, usize), (usize, usize), (usize, usize)),
}

fn nivela(asociados: Asociados, matriz: &Vec<Vec<i64>>) -> i64 {
    match asociados {
        Asociados::Uno(_) => 0,
        Asociados::Dos((i, j), (i2, j2)) => {
            (matriz[i][j] - matriz[i2][j2]).abs()
        },
        Asociados::Cuatro((i, j), (i2, j2), (i3, j3), (i4, j4)) => {
            let columnas = [matriz[i][j], matriz[i2][j2], matriz[i3][j3], matriz[i4][j4]];

            let mut best = std::i64::MAX;

            for i in 0..columnas.len() {
                let mut val = 0;

                for j in 0..columnas.len() {
                    if i == j {
                        continue;
                    }

                    val += (columnas[i] - columnas[j]).abs();
                }

                if val < best {
                    best = val;
                }
            }

            best
        },
    }
}

fn get_asociados((i, j): (usize, usize), filas: usize, cols: usize) -> Asociados {
    let i_reflejo = filas - (i + 1);
    let j_reflejo = cols - (j + 1);

    if i_reflejo == i {
        if j_reflejo == j {
            Asociados::Uno((i_reflejo, j_reflejo))
        } else {
            Asociados::Dos((i, j), (i, j_reflejo))
        }
    } else {
        if j_reflejo == j {
            Asociados::Dos((i, j), (i_reflejo, j))
        } else {
            Asociados::Cuatro((i, j), (i_reflejo, j), (i, j_reflejo), (i_reflejo, j_reflejo))
        }
    }
}

fn solucion(matriz: Vec<Vec<i64>>, cols: usize) -> i64 {
    let mut costo = 0;

    for i in 0..((matriz.len() + 1) / 2) {
        for j in 0..((cols + 1) / 2) {
            let asociados = get_asociados((i, j), matriz.len(), cols);

            let pasos_para_nivelar = nivela(asociados, &matriz);

            costo += pasos_para_nivelar;
        }
    }

    costo
}

fn main() {
    let t: i64 = read();

    for _ in 0..t {
        let nm: Vec<usize> = read_vec();
        let n = nm[0];
        let m = nm[1];

        let mut matriz = Vec::with_capacity(n);

        // leer la matriz
        for _ in 0..n {
            matriz.push(read_vec::<i64>());
        }

        println!("{}", solucion(matriz, m));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
