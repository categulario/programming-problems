use std::fmt;
use std::str::FromStr;
use std::io;

fn cuantas_as_por_bs(b: u64, x: u64) -> u64 {
    x / (b + 1)
}

fn rango(m: u64, n: u64, x: u64) -> u64 {
    assert!(n > m);

    let am = cuantas_as_por_bs(m, x);
    let an = cuantas_as_por_bs(n, x);

    if am == an {
        let cantidad_de_numeros = n - m + 1;
        cantidad_de_numeros * am
    } else if n == m + 1 {
        cuantas_as_por_bs(n, x) + cuantas_as_por_bs(m, x)
    } else {
        let mitad = (n + m) / 2;

        rango(m, mitad, x) + if mitad + 1 == n {
            cuantas_as_por_bs(n, x)
        } else {
            rango(mitad + 1, n, x)
        }
    }
}

fn solve(x: u64, y: u64) -> u64 {
    if y == 1 {
        0
    } else if y == 2 {
        cuantas_as_por_bs(y, x)
    } else {
        let limit = (((x + 1) as f64).sqrt().floor() as u64).min(y);

        ((limit-1) * limit) / 2 + if limit == y {
            0
        } else if limit == y - 1 {
            cuantas_as_por_bs(y, x)
        } else {
            rango(limit + 1, y, x)
        }
    }
}

fn main() {
    let t: u8 = read();

    for _ in 0..t {
        let xy: Vec<u64> = read_vec();
        let x = xy[0];
        let y = xy[1];

        println!("{}", solve(x, y));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
