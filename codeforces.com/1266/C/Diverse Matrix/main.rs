use std::fmt;
use std::str::FromStr;
use std::io;

fn gcd(a: u64, b: u64) -> u64 {
    if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}

fn mcm(a: u64, b: u64) -> u64 {
    a*b/gcd(a, b)
}

fn get_col(matriz: &[Vec<u64>], j: usize) -> Vec<u64> {
    let mut col = Vec::new();

    for fila in matriz {
        col.push(fila[j]);
    }

    col
}

fn vec_gcd(vec: &[u64]) -> u64 {
    if vec.len() == 1 {
        vec[0]
    } else {
        let first = vec[0];

        *(&vec[1..].iter().fold(first, |acc, i| gcd(acc, *i)))
    }
}

fn solve(r: usize, c: usize) -> Option<Vec<Vec<u64>>> {
    if r == 1 && c == 1 {
        return None;
    }

    let mut matriz = vec![vec![0; c]; r];

    // rellenado
    for i in 0..r {
        for j in 0..c {
            matriz[i][j] = mcm((i + 1) as u64, (r + 1 + j) as u64)
        }
    }

    let mut gcds = Vec::with_capacity(r + c);

    for i in 0..r {
        gcds.push(vec_gcd(&matriz[i]));
    }

    for j in 0..c {
        let col = get_col(&matriz, j);
        gcds.push(vec_gcd(&col));
    }

    Some(matriz)
}

fn imprime_bien(sol: Vec<Vec<u64>>) {
    for fila in sol {
        println!("{}", fila
            .into_iter()
            .map(|x| x.to_string())
            .collect::<Vec<_>>()
            .join(" "));
    }
}

fn imprime_alreves(sol: Vec<Vec<u64>>) {
    for j in 0..sol[0].len() {
        println!("{}", get_col(&sol, j).into_iter().map(|x| x.to_string()).collect::<Vec<_>>().join(" "));
    }
}

fn main() {
    let dimensiones: Vec<usize> = read_vec();
    let r = dimensiones[0];
    let c = dimensiones[1];

    let solucion = if r > c {
        solve(c, r)
    } else {
        solve(r, c)
    };

    if let Some(sol) = solucion {
        if r > c {
            imprime_alreves(sol);
        } else {
            imprime_bien(sol);
        }
    } else {
        println!("0");
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
