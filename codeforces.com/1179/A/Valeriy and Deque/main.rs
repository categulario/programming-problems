use std::fmt;
use std::str::FromStr;
use std::io;
use std::collections::VecDeque;

struct State {
    nums: VecDeque<u64>,   // <- el último estado computado
    already_computed: Vec<(u64, u64)>,  // nuestra cache
}

impl State {
    fn new(nums: Vec<u64>) -> State {
        State {
            already_computed: Vec::with_capacity(nums.len()),
            nums: nums.into(),
        }
    }

    fn query(&mut self, m: u64) -> (u64, u64) {
        if m as usize > self.nums.len() {
            if self.already_computed.len() < self.nums.len() {
                for _ in 0..(self.nums.len() - self.already_computed.len()) {
                    self.operacion();
                }

                self.query(m)
            } else {
                let a = self.nums.get(0).unwrap();
                let b = self.nums.get((m as usize - 2)%(self.nums.len()-1) + 1).unwrap();

                (*a, *b)
            }
        } else {
            if let Some(ans) = self.already_computed.get(m as usize - 1) {
                *ans
            } else {
                for _ in 0..(m as usize - self.already_computed.len()) {
                    self.operacion();
                }

                self.query(m)
            }
        }
    }

    fn operacion(&mut self) {
        let a = self.nums.pop_front().unwrap();
        let b = self.nums.pop_front().unwrap();

        self.already_computed.push((a, b));

        if a > b {
            self.nums.push_front(a);
            self.nums.push_back(b);
        } else {
            self.nums.push_front(b);
            self.nums.push_back(a);
        }
    }
}

fn main() {
    let n_q: Vec<u64> = read_vec();
    let nums: Vec<u64> = read_vec();
    let q = n_q[1];
    let mut s = State::new(nums);

    for _ in 0..q {
        let ans = s.query(read());

        println!("{} {}", ans.0, ans.1);
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
