use std::io;

fn solve(h: u64, p: u64) -> u64 {
    let instantes = ((p as f64).log2() + 1.0).floor() as u64;

    if h <= instantes {
        return h;
    }

    let faltantes = 2u64.pow(h as u32) - 1 - (2u64.pow(instantes as u32) - 1);

    instantes + faltantes / p + if faltantes % p == 0 { 0 } else { 1 }
}

fn main() {
    let mut s = String::new();

    read_line(&mut s);

    let t: u64 = s.trim().parse().unwrap();

    for _ in 0..t {
        s.clear();
        read_line(&mut s);

        let mut parts = s.trim().split(" ").map(|x| x.parse().unwrap());

        let h = parts.next().unwrap();
        let p = parts.next().unwrap();

        println!("{}", solve(h, p));
    }
}

fn read_line(s: &mut String) {
    io::stdin().read_line(s).unwrap();
}
