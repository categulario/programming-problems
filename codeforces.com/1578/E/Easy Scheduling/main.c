#include <stdio.h>
#include <math.h>

long solve(long h, long p) {
    long instantes = floor(log2(p) + 1.0);

    if (h <= instantes) {
        return h;
    }

    long faltantes = pow(2, h) - 1 - (pow(2, instantes) - 1);

    return instantes + faltantes / p + (faltantes % p == 0 ? 0 : 1 );
}

int main() {
    long t;
    long h, p;

    scanf("%ld", &t);

    for (int i = 0; i < t; i ++) {
        scanf("%ld %ld", &h, &p);

        printf("%ld\n", solve(h, p));
    }

    return 0;
}
