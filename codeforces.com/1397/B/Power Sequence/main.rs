use std::fmt;
use std::str::FromStr;
use std::io;

fn solve(mut nums: Vec<i64>) -> i64 {
    nums.sort_unstable();

    // dbg!(&nums);

    let ultimo = *nums.last().unwrap() as f64;
    let n = (nums.len() - 1) as f64;

    let prev_c = ultimo.powf(1.0 / n);

    let minc = prev_c.floor() as i64;
    let maxc = prev_c.ceil() as i64;

    // dbg!((0..(n as u64)+1).map(|i| c.pow(i as u32)).collect::<Vec<_>>());

    let res_min = nums
        .iter()
        .enumerate()
        .map(|(i, n)| (*n - minc.pow(i as u32)).abs())
        .sum();
    let res_max = nums
        .iter()
        .enumerate()
        .map(|(i, n)| (*n - maxc.pow(i as u32)).abs())
        .sum();

    if res_max < res_min {
        res_max
    } else {
        res_min
    }
}

fn main() {
    let _n: i64 = read();

    let nums: Vec<i64> = read_vec();

    println!("{}", solve(nums));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
