use std::fmt;
use std::str::FromStr;
use std::io;

fn repaticion_inicial(p: u64, f: u64, cs: u64, cw: u64, s: u64, w: u64) -> (u64, [u64;2], [u64; 2], [u64; 2], u64, u64, u64, u64) {
    // las capacidades ordenadas primero la más grande
    let cap = if p > f {
        [p, f]
    } else {
        [f, p]
    };

    // los pesos ordenados primero el más pequeño
    let (weights, quantities) = if s < w {
        ([s, w], [cs, cw])
    } else {
        ([w, s], [cw, cs])
    };

    // cuántos caben del que pesa menos en el que carga más
    let p1ligero = cap[0] / weights[0];

    if p1ligero <= quantities[0] {
        // esta capacidad está agotada, hay que explorar la capacidad del otro
        // personaje
        let sobrantes = quantities[0] - p1ligero;

        // tratar de meter estos sobrantes en el otro personaje

        // Cuántos de los sobrantes caben
        let p2ligero = cap[1] / weights[0];

        let (cap2, p2ligero) = if p2ligero <= sobrantes {
            // pudimos llenar las capacidades de ambos personajes con puros
            // pesos lugeros
            return (p1ligero + 0 + p2ligero + 0, weights, quantities, cap, p1ligero, 0, p2ligero, 0);
        } else {
            // devuelvo la capacidad que le queda al segundo personaje y la
            // cantidad de elementos del más pequeño que cupieron
            (cap[1] - sobrantes * weights[0], sobrantes)
        };

        // ver cuantos caben de los más pesados en el espacio que queda
        let p2pesado = cap2 / weights[1];

        if p2pesado <= quantities[1] {
            // podemos tomar p2pesado elementos pesados para rellenar el espacio
            // disponible en el segundo personaje
            (p1ligero + 0 + p2ligero + p2pesado, weights, quantities, cap, p1ligero, 0, p2ligero, p2pesado)
        } else {
            // solamente podemos tomar quantities[1] elementos pesados para
            // rellenar el espacio disponible en el segundo jugador
            (p1ligero + 0 + p2ligero + quantities[1], weights, quantities, cap, p1ligero, 0, p2ligero, quantities[1])
        }
    } else {
        // Tomo todos los que puedo (que son quantities[0]) y vuelvo a intentar
        // con el espacio que me queda y la siguiente herramienta
        let p1ligero = quantities[0];
        let p1cap = cap[0] - quantities[0] * weights[0];

        let p1pesado = p1cap / weights[1];

        if p1pesado <= quantities[1] {
            // p1pesado es realmente la cantidad de herramienta pesada que puede
            // llevar el primer jugador, hay que ir por el segundo
            let quantitypesado = quantities[1] - p1pesado;

            // para el segundo jugador ya solamente queda herramienta pesada
            let p2pesado = cap[1] / weights[1];

            if p2pesado <= quantitypesado {
                // tomamos todos los pesados que quedan para el segundo jugador
                (p1ligero + p1pesado + 0 + p2pesado, weights, quantities, cap, p1ligero, p1pesado, 0, p2pesado)
            } else {
                // solamente podemos tomar quantitypesado elementos pesados para
                // el segundo jugador
                (p1ligero + p1pesado + 0 + quantitypesado, weights, quantities, cap, p1ligero, p1pesado, 0, quantitypesado)
            }
        } else {
            // solo puedo llevar quantities[1] herramienta pesada y además ya se
            // acabaron las herramientas para el segundo jugador
            (p1ligero + quantities[1] + 0 + 0, weights, quantities, cap, p1ligero, quantities[1], 0, 0)
        }
    }
}

fn reajuste(c: u64, weights: [u64; 2], quantities: [u64; 2], cap: [u64; 2], p1ligero: u64, p1pesado: u64, p2ligero: u64, p2pesado: u64) -> u64 {
    // TODO condición de paro

    // 
}

fn solve(p: u64, f: u64, cs: u64, cw: u64, s: u64, w: u64) -> u64 {
    let (c, weights, quantities, cap, p1ligero, p1pesado, p2ligero, p2pesado) = repaticion_inicial(p, f, cs, cw, s, w);

    reajuste(c, weights, quantities, cap, p1ligero, p1pesado, p2ligero, p2pesado)
}

fn main() {
    let t: u32 = read();

    for _ in 0..t {
        let capacidades: Vec<u64> = read_vec();
        let cantidades: Vec<u64> = read_vec();
        let pesos: Vec<u64> = read_vec();

        println!("{}", solve(capacidades[0], capacidades[1], cantidades[0], cantidades[1], pesos[0], pesos[1]));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
