use std::fmt;
use std::str::FromStr;
use std::io;

fn solve(n: u64, k: u64) -> u64 {
    let target = if n > k {
        if n % k == 0 {
            return 1;
        } else {
            (n / k + 1) * k
        }
    } else if n == k {
        return 1;
    } else {
        if k % n == 0 {
            return k / n;
        } else {
            k
        }
    };

    target / n + 1
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        let nk: Vec<u64> = read_vec();
        let n = nk[0];
        let k = nk[1];

        println!("{}", solve(n, k));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read<T: FromStr>() -> T
where <T as FromStr>::Err: fmt::Debug
{
    read_line().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
where <T as FromStr>::Err: fmt::Debug
{
    read_line().split(" ").map(|x| x.parse().unwrap()).collect()
}
