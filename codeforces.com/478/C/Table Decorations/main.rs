use std::fmt;
use std::str::FromStr;
use std::io;
use std::cmp::Reverse;

fn ordenados(nums: Vec<u64>) -> Vec<u64> {
    let mut nums = nums;

    nums.sort_by_key(|&a| Reverse(a));

    nums
}

fn solve(nums: Vec<u64>) -> u64 {
    if nums.iter().filter(|&a| *a == 0).count() >= 2 {
        return 0;
    }

    if nums.iter().sum::<u64>() < 3 {
        return 0;
    }

    let nums = ordenados(nums);

    let c = nums[0] / 4;

    if c == 0 {  // se pudo quitar la mitad
        let m = nums[0] / 2;

        if m == 0 {
            if nums.iter().sum::<u64>() == 3 {
                1
            } else {
                0
            }
        } else {
            m + solve(vec![nums[0] - m * 2, nums[1] - m, nums[2]])
        }
    } else { // sí tiene cuarta parte
        let c = c.min(nums[1]);

        c + solve(vec![nums[0] - c * 2, nums[1] - c, nums[2]])
    }
}

fn main() {
    let rgb: Vec<u64> = read_vec();

    println!("{}", solve(rgb));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
