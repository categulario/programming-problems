use std::io;
use std::str::FromStr;
use std::fmt;

const LIM: u64 = 500_000;

fn odd_sum(a: u64, b: u64) -> u64 {
    ( (a-1)/2 ..= (b-1)/2 )
        .map(|x| 2*x+1)
        .fold(0, |acc, x| acc+x)
}

/// Encuentra la secuencia de impares consecutivos que suman al número dado
fn find_sequence(n: u64, start: u64) -> Option<(u64, u64)> {
    let mut i = start;
    let mut j = start;
    let mut sum = start;

    loop {
        if sum < n && j < LIM {
            j += 2;
            sum += j;
        } else if sum > n && i < j {
            sum -= i;
            i += 2;
        } else if sum == n {
            return Some((i, j));
        } else {
            break;
        }
    }

    None
}

fn main() {
    let _n = read::<u64>();
    let even_items = read_vec::<u64>();

    let mut sol = Vec::new();
    let mut range_start = 1;
    let mut start = 3;
    let mut found_sol = true;

    for item in even_items {
        if let Some((a, b)) = find_sequence(item, start) {
            sol.push(odd_sum(range_start, a-2));
            sol.push(item);

            start = b + 4;
            range_start = b + 2;
        } else {
            found_sol = false;
            break;
        }
    }

    if found_sol {
        println!("Yes");
        println!("{}", sol.into_iter().map(|x| x.to_string()).collect::<Vec<_>>().join(" "));
    } else {
        println!("No");
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
