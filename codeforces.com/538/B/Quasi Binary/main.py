def not_all_zero(vec):
    return any(vec)


def take_row(vec):
    n = 0

    for i, item in enumerate(vec):
        if item > 0:
            n += 10**i
            vec[i] -= 1

    return n


def main():
    n = int(input())
    digits = []

    while n > 0:
        digits.append(n % 10)
        n //= 10

    nums = []

    while not_all_zero(digits):
        nums.append(take_row(digits))

    print(len(nums))
    print(' '.join(str(i) for i in nums))


if __name__ == '__main__':
    main()
