use std::io;
use std::str::FromStr;
use std::fmt;

fn not_all_zero(vec: &[u32]) -> bool {
    vec.iter().any(|n| *n != 0)
}

fn take_row(vec: &mut [u32]) -> u32 {
    let mut n = 0;

    for (i, item) in vec.iter_mut().enumerate() {
        if *item > 0 {
            n += 10_u32.pow(i as u32);
            *item -= 1;
        }
    }

    n
}

fn main() {
    let mut n: u32 = read();
    let mut digits = Vec::new();

    while n > 0 {
        digits.push(n % 10);
        n /= 10;
    }

    let mut nums = Vec::new();

    while not_all_zero(&digits) {
        nums.push(take_row(&mut digits));
    }

    println!("{}", nums.len());
    println!("{}",
        nums
            .iter()
            .map(|n| n.to_string())
            .collect::<Vec<_>>()
            .join(" ")
    );
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}
