-module(main).
-export([hello/0]).
-compile([debug_info]).

hello() ->
    io:format("hello world~n").
