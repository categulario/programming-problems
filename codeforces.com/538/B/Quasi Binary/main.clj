(def n (Integer/parseInt (read-line)))

(defn digits [arg]
  (if (== arg 0)
    '()
    (conj (digits (int (/ arg 10))) (mod arg 10))))

(defn all-zero [arg]
  (if (= (first arg) nil)
    true
    (and (= (first arg) 0) (all-zero (rest arg)))))

(defn decr-row [row]
  (if (= (first row) nil)
    '()
    (let [first-item (first row)]
      (conj (decr-row (rest row)) (if (= first-item 0) 0 (dec first-item))))))

(defn get-row [arg]
  (if (= (first arg) nil)
    '()
    (conj (get-row (rest arg)) (if (= (first arg) 0) 0 1))))

(defn get-rows [arg]
  (if (all-zero arg)
    '()
    (conj (get-rows (decr-row arg)) (get-row arg))))

(defn as-number [row, exp]
  (if (= (first row) nil)
    0
    (+ (as-number (rest row) (inc exp)) (* (first row) (int (Math/pow 10 exp))))))

(defn as-number-starter [row]
  (as-number row 0))

(def rows (get-rows (digits n)))

(println (count rows))
(apply println (map as-number-starter rows))
