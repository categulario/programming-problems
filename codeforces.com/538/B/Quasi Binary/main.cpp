#include<stdio.h>
#include<math.h>

bool not_all_zero(int* vec) {
    for (int i = 0; i < 10; i++) {
        if (vec[i] != 0) {
            return true;
        }
    }

    return false;
}

int take_row(int* vec) {
    int n = 0;

    for (int i = 0; i < 10; i++) {
        if (vec[i] > 0) {
            n += pow(10, i);
            vec[i] -= 1;
        }
    }

    return n;
}

void init(int* vec) {
    for (int i = 0; i < 10; i ++) {
        vec[i] = 0;
    }
}

int main() {
    int n;

    scanf("%d", &n);

    int digits[10];
    init(digits);

    int i = 0;

    while (n > 0) {
        digits[i] = n % 10;
        n /= 10;
        i ++;
    }

    int nums[10];
    init(nums);

    int cont = 0;

    while (not_all_zero(digits)) {
        nums[cont] = take_row(digits);
        cont++;
    }

    printf("%d\n", cont);

    for (int i = 0; i < cont; i++) {
        if (i == cont-1) {
            printf("%d\n", nums[i]);
        } else {
            printf("%d ", nums[i]);
        }
    }
}
