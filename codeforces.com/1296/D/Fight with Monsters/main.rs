use std::fmt;
use std::str::FromStr;
use std::io;

fn calcula_las_ks(monstruo: u64, a: u64, b: u64) -> u64 {
    let s = a + b;

    (a + b) / a
}

fn cuantas_ks(monstruo: u64, a: u64, b: u64) -> u64 {
    let r = monstruo % (a + b);

    if r == 0 {
        (a + b) / a
    } else if r > a {
        if r % a == 0 {
            r / a - 1
        } else {
            r / a
        }
    } else {
        0
    }
}

fn solve(a: u64, b: u64, k: u64, monsters: Vec<u64>) -> u64 {
    let mut costos: Vec<u64> = monsters.into_iter().map(|m| cuantas_ks(m, a, b)).collect();

    // orden ascendente de los costos en ks de cada mostro
    costos.sort_unstable();

    let mut k = k;

    costos.into_iter().filter(|&ks| {
        if k >= ks { // si todavía nos quedan ks
            k -= ks; // disminuimos las k
            return true; // el monstruo si va
        } else {
            return false; // el monstruo no va
        }
    }).fuse().count() as u64
}

fn main() {
    let nabk: Vec<u64> = read_vec();
    let monsters: Vec<u64> = read_vec();

    let a = nabk[1];
    let b = nabk[2];
    let k = nabk[3];

    println!("{}", solve(a, b, k, monsters));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
