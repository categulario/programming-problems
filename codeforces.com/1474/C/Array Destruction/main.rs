use std::fmt;
use std::str::FromStr;
use std::io;
use std::collections::{HashMap, HashSet};
use std::cmp::Ordering;

type Map = HashMap<u64, HashSet<(u64, u64)>>;

fn remove(vec: &mut Vec<u64>, a: u64) -> bool {
    if let Ok(index) = vec.binary_search(&a) {
        vec.remove(index);

        true
    } else {
        false
    }
}

fn get_all_sums(a: &[u64]) -> Map {
    let mut res = HashMap::new();

    for i in 2..a.len() {
        if res.contains_key(&a[i]) {
            continue;
        }

        let mut j = 0;
        let mut k: i32 = i as i32 - 1;

        loop {
            match (a[j] + a[k as usize]).cmp(&a[i]) {
                Ordering::Less => {
                    j += 1;
                },
                Ordering::Greater => {
                    k -= 1;
                },
                Ordering::Equal => {
                    // añadimos este par al hashmap
                    let set = res.entry(a[i]).or_insert_with(|| HashSet::new());

                    set.insert((a[k as usize], a[j]));

                    j += 1;
                    k -= 1;
                },
            }

            if k <= j as i32 || k < 0 || j >= i {
                break;
            }
        }
    }

    res
}

fn recursion(faltantes: Vec<u64>, grande: u64, all_sums: &Map) -> Option<(u64, Vec<(u64, u64)>)> {
    if faltantes.len() == 1 {
        return Some((faltantes[0], vec![]));
    }

    if !all_sums.contains_key(&grande) {
        return None;
    }

    for &(a, b) in all_sums[&grande].iter() {
        let mut menos_faltantes = faltantes.clone();

        if !remove(&mut menos_faltantes, a) {
            continue;
        }
        if !remove(&mut menos_faltantes, b) {
            continue;
        }

        if let Some((x, mut arbol)) = recursion(menos_faltantes, a, all_sums) {
            // devuelve una solución construida con estos valores y los que
            // devolvió la recursón
            arbol.push((a, b));

            return Some((x, arbol));
        }
    }

    None
}

fn solve(mut a: Vec<u64>) -> Option<(u64, Vec<(u64, u64)>)> {
    if a.len() == 2 {
        return Some((a.iter().sum(), vec![(a[0], a[1])]));
    }

    a.sort();

    let all_sums = get_all_sums(&a);

    // escoger el más grande A
    // recursivamente intentar encontrar una suma que de A
    // de los sumandos escoger el más grande
    // recursión hasta que se acabe el arreglo o se encuentre un número que no
    // está en el hashmap
    let max = a.last().copied().unwrap();

    let mut faltantes = a.clone();

    remove(&mut faltantes, max);

    if let Some((sobrante, mut arbol)) = recursion(faltantes, max, &all_sums) {
        arbol.push((max, sobrante));

        Some((sobrante + max, arbol))
    } else {
        None
    }
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        let _n: u64 = read();
        let a: Vec<u64> = read_vec();

        if let Some((x, steps)) = solve(a) {
            println!("YES");
            println!("{}", x);

            for step in steps.iter().rev() {
                println!("{} {}", step.0, step.1);
            }
        } else {
            println!("NO");
        }
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
