use std::fmt;
use std::str::FromStr;
use std::io;

struct Heap {
    heap: Vec<u64>,
    coin_count: u64,
    limit: usize,
}

impl Heap {
    fn with_size_limit(limit: usize) -> Heap {
        Heap {
            heap: Vec::with_capacity(limit),
            coin_count: 0,
            limit,
        }
    }

    fn coin_count(&self) -> u64 {
        self.coin_count
    }

    fn push(&mut self, coins: u64) {
        // add the coins to the heap, and if the length of the heap exceeds the
        // limit then discard the lowest one
        unimplemented!()
    }
}

fn solve(powers: Vec<u64>, coins: Vec<u64>, k: usize) -> Vec<u64> {
    let mut powers_and_coins: Vec<_> = powers.into_iter().zip(coins.into_iter()).enumerate().collect();
    let mut answers = vec![0; powers_and_coins.len()];

    powers_and_coins.sort_unstable_by(|a, b| (a.1.0).cmp(&b.1.0));

    let mut heap = Heap::with_size_limit(k);

    for (index, (own_power, own_coins)) in powers_and_coins.iter() {
        answers[*index] = own_coins + heap.coin_count();

        // when we're done computing things for this knight we just push it to
        // the heap
        heap.push(*own_coins);
    }

    answers
}

fn main() {
    let n_k: Vec<usize> = read_vec();
    let k = n_k[1];

    let powers: Vec<u64> = read_vec();
    let coins: Vec<u64> = read_vec();

    let results = solve(powers, coins, k);

    println!("{}", results
        .into_iter()
        .map(|n| n.to_string())
        .collect::<Vec<_>>()
        .join(" "));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
