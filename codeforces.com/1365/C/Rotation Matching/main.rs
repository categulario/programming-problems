use std::fmt;
use std::str::FromStr;
use std::io;
use std::collections::HashMap;

fn modulo(a: i64, b: i64) -> i64 {
    ((a % b) + b) % b
}

fn solucion(arreglo_a: &[i64], arreglo_b: &[i64]) -> i64 {
    let mut mapa = HashMap::new();
    let mut max_matching = 1;
    let mut dists = vec![0; arreglo_a.len()];

    // O(n)
    for (i, (a, b)) in arreglo_a.iter().zip(arreglo_b).enumerate() {
        let dist_a = (a - 1) - i as i64;

        dists[(a - 1) as usize] += dist_a;

        let dist_b = (b - 1) - i as i64;

        dists[(b - 1) as usize] -= dist_b;

        dists[(b - 1) as usize] = modulo(dists[(b - 1) as usize], arreglo_a.len() as i64);
    }

    for dist in dists {
        let entry = mapa.entry(dist).or_insert(0);  // &mut usize

        *entry += 1;

        if *entry > max_matching {
            max_matching = *entry;
        }
    }

    max_matching
}

fn main() {
    let _size: usize = read();

    let arreglo_a: Vec<i64> = read_vec();
    let arreglo_b: Vec<i64> = read_vec();

    println!("{}", solucion(&arreglo_a, &arreglo_b));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
