use std::fmt;
use std::str::FromStr;
use std::io;

fn make_arr(i: usize, j: usize, k: usize, l: usize) -> Vec<usize> {
    let mut salida = Vec::with_capacity(4);

    salida.push(i);

    if j != i {
        salida.push(j);
    }

    if k != j {
        salida.push(k);
    }

    salida.push(l);

    salida
}

fn improve(p: i64, q: i64, r: i64, arr: Vec<usize>, data: &[i64]) -> (usize, usize, usize) {
    let mut max = p * data[arr[0]] + q * data[arr[0]] + r * data[arr[0]];
    let mut maxi = 0;
    let mut maxj = 0;
    let mut maxk = 0;

    for ii in 0..arr.len() {
        for jj in ii..arr.len() {
            for kk in jj..arr.len() {
                let test = p * data[arr[ii]] + q * data[arr[jj]] + r * data[arr[kk]];

                if test > max {
                    max = test;
                    maxi = arr[ii];
                    maxj = arr[jj];
                    maxk = arr[kk];
                }
            }
        }
    }

    (maxi, maxj, maxk)
}

fn solucion(p: i64, q: i64, r: i64, data: &[i64]) -> i64 {
    // Caso 1
    let mut i = 0;
    let mut j = 0;
    let mut k = 0;

    // Suponiendo el caso k
    for l in 1..data.len() {
        let arr = make_arr(i, j, k, l);
        let t = improve(p, q, r, arr, &data);

        i = t.0;
        j = t.1;
        k = t.2;
    }

    p * data[i] + q * data[j] + r * data[k]
}

fn main() {
    let data: Vec<i64> = read_vec();

    let p = data[1];
    let q = data[2];
    let r = data[3];
    let data: Vec<i64> = read_vec();

    println!("{}", solucion(p, q, r, &data));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
