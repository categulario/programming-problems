use std::fmt;
use std::str::FromStr;
use std::io;
use std::collections::{HashMap, BinaryHeap, HashSet};
use std::cmp::{Reverse, Ordering};

struct Grafo {
    vertices: HashMap<u32, HashMap<u32, u32>>,
}

impl Grafo {
    fn new(n: u32) -> Grafo {
        Grafo {
            vertices: HashMap::with_capacity(n as usize),
        }
    }

    fn add_edge(&mut self, vertice1: u32, vertice2: u32, costo: u32) {
        let entrada1 = self.vertices.entry(vertice1).or_insert(HashMap::new());

        if let Some(&costo_anterior) = entrada1.get(&vertice2) {
            if costo < costo_anterior {
                entrada1.insert(vertice2, costo);
            }
        } else {
            entrada1.insert(vertice2, costo);
        }

        let entrada2 = self.vertices.entry(vertice2).or_insert(HashMap::new());

        if let Some(&costo_anterior) = entrada2.get(&vertice1) {
            if costo < costo_anterior {
                entrada2.insert(vertice1, costo);
            }
        } else {
            entrada2.insert(vertice1, costo);
        }
    }

    fn adyacentes<'a>(&'a self, nodo: u32) -> impl Iterator<Item=(&'a u32, &'a u32)> {
        self.vertices[&nodo].iter()
    }
}

#[derive(PartialEq, Eq)]
struct PriorityItem {
    nodo: u32,
    costo: u32,
    prev: Option<u32>,
}

impl PartialOrd for PriorityItem {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.costo.partial_cmp(&other.costo)
    }
}

impl Ord for PriorityItem {
    fn cmp(&self, other: &Self) -> Ordering {
        self.costo.cmp(&other.costo)
    }
}

impl PriorityItem {
    fn new(nodo: u32, costo: u32, prev: Option<u32>) -> PriorityItem {
        PriorityItem {
            nodo, costo, prev,
        }
    }
}

fn ruta_mas_corta(grafo: &Grafo, desde: u32, hasta: u32) -> Option<Vec<u32>> {
    let mut cola = BinaryHeap::new();
    let mut visitados: HashSet<u32> = HashSet::new();
    let mut rutas: HashMap<u32, u32> = HashMap::new();

    cola.push(Reverse(PriorityItem::new(desde, 0, None)));

    loop {
        if let Some(Reverse(item)) = cola.pop() {
            // sacamos un nodo que ya estaba visitado
            if visitados.contains(&item.nodo) {
                continue;
            }

            // expandir los no visitados y añadir a la cola
            for (&nodo, costo) in grafo.adyacentes(item.nodo).filter(|(nodo, _)| !visitados.contains(nodo)) {
                cola.push(Reverse(PriorityItem::new(nodo, item.costo + costo, Some(item.nodo))));
            }

            if item.nodo == hasta {
                let mut ruta = vec![item.nodo];
                let mut prev: Option<u32> = item.prev;

                while let Some(n) = prev {
                    ruta.insert(0, n);
                    prev = rutas.get(&n).copied();
                }

                return Some(ruta);
            } else {
                // actualizar los visitados
                visitados.insert(item.nodo);

                if let Some(p) = item.prev {
                    rutas.insert(item.nodo, p);
                }
            }
        } else {
            break;
        }
    }

    None
}

fn main() {
    let nm: Vec<u32> = read_vec();
    let n = nm[0];
    let m = nm[1];

    let mut grafo = Grafo::new(nm[0]);

    for _ in 0..m {
        let arista: Vec<u32> = read_vec();

        grafo.add_edge(arista[0], arista[1], arista[2]);
    }

    if let Some(ruta) = ruta_mas_corta(&grafo, 1, n) {
        println!("{}", ruta.into_iter().map(|e| e.to_string()).collect::<Vec<_>>().join(" "));
    } else {
        println!("-1");
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();

    if let Err(_) = io::stdin().read_line(&mut pre_n) {
        println!("Error al leer la linea");
    }

    pre_n
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").filter_map(|x| {
        x.trim().parse().ok()
    }).collect()
}
