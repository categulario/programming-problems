use std::fmt;
use std::str::FromStr;
use std::io;

fn se_puede(n: u64) -> bool {
    for i in 0..11 {
        if i * 111 > n {
            break;
        }

        if (n - 111*i) % 11 == 0 {
            return true;
        }
    }

    false
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        let n: u64 = read();

        if se_puede(n) {
            println!("YES");
        } else {
            println!("NO");
        }
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read<T: FromStr>() -> T
where <T as FromStr>::Err: fmt::Debug
{
    read_line().parse().unwrap()
}
