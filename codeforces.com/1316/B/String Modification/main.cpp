#include <stdio.h>
#include <string.h>
#include <iostream>

using namespace std;

string reversa(string cadena) {
    string salida = cadena;
    int fin;

    for (int i = 0; i < cadena.length(); i++) {
        fin = cadena.length() - i - 1;
        salida[fin] = cadena[i];
    }

    return salida;
}

string voltear(int k, string cadena) {
    string salida = cadena.substr(k-1);

    if ((k%2 + cadena.length()%2)%2 == 0) {
        salida += reversa(cadena.substr(0, k-1));
    } else {
        salida += cadena.substr(0, k-1);
    }

    return salida;
}

void resolver(int longitud, string cadena) {
    string cadena_mas_pequena = cadena;
    int su_k = 1;

    for (int k = 2; k <= longitud; k++) {
        string nueva_cadena = voltear(k, cadena);

        if (nueva_cadena < cadena_mas_pequena) {
            cadena_mas_pequena = nueva_cadena;
            su_k = k;
        }
    }

    cout << cadena_mas_pequena << endl;
    cout << su_k << endl;
}

int main()  {
    int t;
    int longitud;
    string cadena, aux;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        scanf("%d", &longitud);

        // saltar el salto de línea que queda
        cin.ignore();

        getline(cin, cadena);

        resolver(longitud, cadena);
    }
}
