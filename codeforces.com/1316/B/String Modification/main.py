import sys


def reversa(cadena):
    salida = list(cadena)

    for i in range(len(cadena)):
        fin = len(cadena) - i -1

        salida[fin] = cadena[i]

    return ''.join(salida)


# k par cadena par => invertido
# k impar cadena impar => invertido
# k par cadena impar => bien
# k impar cadena par => bien
def voltear(k, cadena):
    salida = cadena[k-1:]

    if (k%2 + len(cadena)%2)%2 == 0:
        salida += reversa(cadena[:k-1])
    else:
        salida += cadena[:k-1]

    return salida


def resolver(longitud, cadena):
    cadena_mas_pequeña = cadena
    su_k = 1

    for k in range(2, len(cadena)+1):
        nueva_cadena = voltear(k, cadena)

        if nueva_cadena < cadena_mas_pequeña:
            cadena_mas_pequeña = nueva_cadena
            su_k = k

    print(cadena_mas_pequeña)
    print(su_k)


if __name__ == '__main__':
    t = int(sys.stdin.readline().strip())

    for i in range(t):
        longitud = int(sys.stdin.readline().strip())
        cadena = sys.stdin.readline().strip()

        resolver(longitud, cadena)
