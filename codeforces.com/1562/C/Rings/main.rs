use std::fmt;
use std::str::FromStr;
use std::io;

fn solve(s: String) -> (u64, u64, u64, u64) {
    let n = s.len() as u64;

    if let Some(i) = s.find('0') {
        dbg!(i);
        let i = i as u64;

        if i >= n / 2 {
            // después de la mitad
            (1, i + 1, 1, i)
        } else {
            // antes de la mitad
            (i + 1, n, i + 2, n)
        }
    } else {
        // hay puros unos
        (1, n - 1, 2, n)
    }
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        let _: u64 = read();
        let s = read_line();

        let (l1, l2, r1, r2) = solve(s);

        println!("{} {} {} {}", l1, l2, r1, r2);
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read<T: FromStr>() -> T
where <T as FromStr>::Err: fmt::Debug
{
    read_line().parse().unwrap()
}
