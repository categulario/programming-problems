def caso(n, permutacion, indexes):
    ocupados = [False for i in range(n)]

    for num_a_ubicar in range(n): # Para ubicar cada número
        r = [False for i in range(n)]
        count = [0 for i in range(n)]
        max_count = -1
        max_indexes = []

        # crear el arreglo r
        for i in range(n):
            siguiente_disponible = 'x'

            for j in range(i, n):
                if not ocupados[j]:
                    siguiente_disponible = j
                    break

            r[i] = siguiente_disponible

            if siguiente_disponible != 'x':
                count[siguiente_disponible] += 1

                if count[siguiente_disponible] > max_count:
                    max_count = count[siguiente_disponible]
                    max_indexes = [siguiente_disponible]
                elif count[siguiente_disponible] == max_count:
                    max_indexes.append(siguiente_disponible)

        if not indexes[num_a_ubicar+1] in max_indexes:
            return False
        else:
            ocupados[indexes[num_a_ubicar+1]] = True

    return True


def main():
    t = int(input())

    for i in range(t):
        n = int(input())
        permutacion = list(map(int, input().strip().split(' ')))

        indexes = dict()

        for i, num in enumerate(permutacion):
            indexes[num] = i

        if caso(n, permutacion, indexes):
            print('Yes')
        else:
            print('No')


if __name__ == '__main__':
    main()
