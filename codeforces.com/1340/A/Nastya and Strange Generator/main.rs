use std::collections::HashMap;

fn caso(n: u32, permutacion: Vec<u32>) -> bool {
    let indexes = HashMap::new();

    for (i, e) in permutacion.iter().enumerate() {
        indexes.insert(e, i);
    }

    ocupados = [False for i in range(n)]

    for num_a_ubicar in range(n): # Para ubicar cada número
        r = [False for i in range(n)]
        count = [0 for i in range(n)]
        max_count = -1
        max_indexes = []

        # crear el arreglo r
        for i in range(n):
            siguiente_disponible = 'x'

            for j in range(i, n):
                if not ocupados[j]:
                    siguiente_disponible = j
                    break

            r[i] = siguiente_disponible

            if siguiente_disponible != 'x':
                count[siguiente_disponible] += 1

                if count[siguiente_disponible] > max_count:
                    max_count = count[siguiente_disponible]
                    max_indexes = [siguiente_disponible]
                elif count[siguiente_disponible] == max_count:
                    max_indexes.append(siguiente_disponible)

        if not indexes[num_a_ubicar+1] in max_indexes:
            return False
        else:
            ocupados[indexes[num_a_ubicar+1]] = True

    return True
}

fn main() {
    let t: u32 = read();

    for i in (0..t) {
        let n: u32 = read();
        let permutacion: Vec<u32> = read_vec();

        if caso(n, permutacion) {
            println!("Yes");
        } else {
            println!("No");
        }
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
