use std::fmt;
use std::str::FromStr;
use std::io;

#[derive(PartialEq)]
enum AuxCell {
    Wall,
    Unvisited,
    Root,
    Cell {
        parent: [usize; 2],
        num_child: usize,
    },
}

use AuxCell::*;

fn solve(maze: Vec<Vec<bool>>, k: usize, width: usize) -> Vec<Vec<char>> {
    let mut aux_maze: Vec<Vec<AuxCell>> = Vec::with_capacity(maze.len());
    let mut solution: Vec<Vec<char>> = Vec::with_capacity(maze.len());
    let mut root_found = false;
    let mut root_coordinates = [0, 0];

    // Calcular el laberinto auxiliar inicial, y la solución inicial
    for (i, row) in maze.iter().enumerate() {
        aux_maze.push(row.iter().enumerate().map(|(j, &c)| {
            if c { // muro
                Wall
            } else { // casilla libre
                if root_found {
                    Unvisited
                } else {
                    root_found = true;
                    root_coordinates = [i, j];

                    Root
                }
            }
        }).collect());
        solution.push(row.iter().map(|&c| if c { '#' } else { '.' }).collect());
    }

    // exploración para encontrar las hojas
    let mut hojas = Vec::new();
    let mut pila = vec![root_coordinates];

    loop {
        if let Some([i, j]) = pila.pop() {
            let mut n_child = 0;

            // arriba
            if i > 0 && aux_maze[i-1][j] == Unvisited {
                aux_maze[i-1][j] = AuxCell::Cell {
                    parent: [i, j],
                    num_child: 0,
                };
                pila.push([i-1, j]);
                n_child += 1;
            }

            // derecha
            if j < width-1 && aux_maze[i][j+1] == Unvisited {
                aux_maze[i][j+1] = AuxCell::Cell {
                    parent: [i, j],
                    num_child: 0,
                };
                pila.push([i, j+1]);
                n_child += 1;
            }

            // abajo
            if i < maze.len() - 1 && aux_maze[i+1][j] == Unvisited {
                aux_maze[i+1][j] = AuxCell::Cell {
                    parent: [i, j],
                    num_child: 0,
                };
                pila.push([i+1, j]);
                n_child += 1;
            }

            // izquierda
            if j > 0 && aux_maze[i][j-1] == Unvisited {
                aux_maze[i][j-1] = AuxCell::Cell {
                    parent: [i, j],
                    num_child: 0,
                };
                pila.push([i, j-1]);
                n_child += 1;
            }

            if n_child == 0 {
                hojas.push([i, j]);
            }

            match &mut aux_maze[i][j] {
                Cell { num_child, .. } => {
                    *num_child = n_child;
                },
                _ => {}
            }
        } else {
            break;
        }
    }

    // convertir todas las hojas necesarias, hasta cubrir 4 en total
    for _ in 0..k {
        if let Some([i, j]) = hojas.pop() {
            // marcar esta hoja como muro
            solution[i][j] = 'X';

            let parent = if let Cell { parent, .. } = &aux_maze[i][j] {
                *parent
            } else { panic!("esto no debería pasar") };

            if let Cell { num_child, .. } = &mut aux_maze[parent[0]][parent[1]] {
                // le restamos un hijo a su padre
                *num_child -= 1;

                // si el padre ya no tiene hijos lo ponemos como hoja
                if *num_child == 0 {
                    hojas.push(parent);
                }
            }
        } else {
            panic!("AAAAAAAAAAAAAAAAiuaaaa");
        }
    }

    solution
}

fn main() {
    let nmk: Vec<usize> = read_vec();
    let height = nmk[0];
    let width = nmk[1];
    let k = nmk[2];

    let mut maze = Vec::with_capacity(height as usize);

    for _ in 0..height {
        let line: Vec<_> = read_line().chars().map(|c| c == '#').collect(); // devuelve un vector

        maze.push(line);
    }

    let output = solve(maze, k, width);

    for line in output {
        let l: String = line.into_iter().collect(); // devuelve un string

        println!("{}", l);
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().into()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
