def solve_case(n):
    k = 0
    pow = 1  # 2^k - 1
    escalera = 1

    while escalera <= n:
        k += 1
        n -= escalera

        escalera = escalera * 2 + (pow+1)*(pow+1)
        pow = pow * 2 + 1

    return k


def main():
    cases = int(input())

    for _ in range(cases):
        num = int(input())

        print("{}".format(solve_case(num)))


main()
