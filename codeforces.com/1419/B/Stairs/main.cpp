#include<stdio.h>

long solve_case(long n) {
    long k = 0;
    long pow = 1;  // 2^k - 1
    long escalera = 1;

    printf("%ld %ld\n", escalera, n);

    while (escalera <= n) {
        k += 1;
        n -= escalera;

        escalera = escalera * 2 + (pow+1)*(pow+1);
        pow = pow * 2 + 1;
    }

    return k;
}

int main() {
    int cases;
    long num;

    scanf("%d", &cases);

    for (int i = 0; i < cases; i++) {
        scanf("%ld", &num);

        printf("%ld\n", solve_case(num));
    }
}
