use std::fmt;
use std::str::FromStr;
use std::io;

enum Caso {
    Rango(u32),
    RangoEIguales{
        rango: u32,
        iguales: u32,
    },
    RangoYNumero{
        rango: u32,
        numero: u32,
    },
    DosIguales(u32),
    Fin,
}

struct Iterador {
    state: Caso,
}

impl Iterador {
    fn new(n: u32) -> Iterador {
        Iterador {
            state: Caso::Rango(n),
        }
    }
}

impl Iterator for Iterador {
    type Item = (u32, u32);

    fn next(&mut self) -> Option<Self::Item> {
        match self.state {
            Caso::Rango(n) => {
                if n > 2 {
                    if n > 3 {
                        self.state = Caso::RangoEIguales {
                            rango: n - 3,
                            iguales: n - 1,
                        };
                    } else { // n == 3
                        self.state = Caso::DosIguales(n - 1);
                    }

                    Some((n - 2, n))
                } else {
                    self.state = Caso::Fin;

                    Some((1, 2))
                }
            },
            Caso::RangoEIguales{ rango, iguales } => {
                self.state = Caso::RangoYNumero{ rango, numero: iguales };

                Some((iguales, iguales))
            },
            Caso::RangoYNumero{ rango, numero } => {
                if rango == 1 {
                    self.state = Caso::Fin;
                } else {
                    self.state = Caso::RangoYNumero { rango: rango-1, numero: numero-1 };
                }

                Some((rango, numero))
            },
            Caso::DosIguales(n) => {
                self.state = Caso::Fin;

                Some((n, n))
            }
            Caso::Fin => None,
        }
    }
}

fn solucion(n: u32) {
    let iter = Iterador::new(n);

    println!("2");

    for (a, b) in iter {
        println!("{} {}", a, b);
    }
}

fn main() {
    let t: u32 = read();

    for _ in 0..t {
        let n: u32 = read();

        solucion(n);
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}
