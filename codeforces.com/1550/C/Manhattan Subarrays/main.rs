use std::fmt;
use std::str::FromStr;
use std::io;
use std::cmp::Ordering;

fn triplet_is_good(p: (u64, u64), q: (u64, u64), r: (u64, u64)) -> bool {
    match q.0.cmp(&p.0) {
        Ordering::Equal => false,
        Ordering::Less => r.0 > q.0,
        Ordering::Greater => r.0 < q.0,
    }
}

fn cuatriplet_is_good(p: (u64, u64), q: (u64, u64), r: (u64, u64), s: (u64, u64)) -> bool {
    triplet_is_good(p, q, s) && triplet_is_good(p, r, s) &&
    s.0 > p.0.min(q.0).min(r.0) && s.0 < p.0.max(q.0).max(r.0)
}

fn solve(arr: &[u64]) -> u64 {
    let iter = arr
        .iter()
        .enumerate()
        .map(|(i, a)| (*a, i as u64));

    let good_triplets: Vec<_> = iter.clone()
        .zip(iter.clone().skip(1))
        .zip(iter.clone().skip(2))
        .map(|((i, j), k)| triplet_is_good(i, j, k))
        .collect();

    let good_cuatriplets = iter.clone()
        .zip(iter.clone().skip(1))
        .zip(iter.clone().skip(2))
        .zip(iter.clone().skip(3))
        .zip(good_triplets.iter())
        .zip(good_triplets.iter().skip(1))
        .filter(|(((((p, q), r), s), t1), t2)| **t1 && **t2 && cuatriplet_is_good(*p, *q, *r, *s))
        .count();

    let tam_3 = good_triplets.iter().filter(|g| **g).count();

    //  1          2              3       4
    (arr.len() + arr.len() - 1 + tam_3 + good_cuatriplets) as u64
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        let _n: u64 = read();
        let arr: Vec<u64> = read_vec();

        println!("{}", solve(&arr));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read<T: FromStr>() -> T
where <T as FromStr>::Err: fmt::Debug
{
    read_line().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
where <T as FromStr>::Err: fmt::Debug
{
    read_line().split(" ").map(|x| x.parse().unwrap()).collect()
}
