use std::fmt;
use std::str::FromStr;
use std::io;

/// K es la cantidad de cifras menos 1 de n
fn es_puros_unos(mut n: u64) -> (bool, u32) {
    let mut k = 0;
    let mut puros_unos = true;

    while n > 0 {
        let d = n % 2;

        if d == 0 {
            puros_unos = false;
        }

        k += 1;
        n = n >> 1;
    }

    (puros_unos, k - 1)
}

#[inline]
fn neg(n: u64) -> u64 {
    if n == 0 {
        1
    } else {
        0
    }
}

fn caso_puros_unos(k: u32) -> u64 {
    if (k + 1) % 2 == 0 { // hay una cantidad par de unos
        // intercalamos
        let mut r = 0;
        let mut b = 1;

        for i in 0..(k+1) {
            r += 2_u64.pow(i as u32) * b;

            b = neg(b);
        }

        r
    } else {  // hay una cantidad impar de unos
        1
    }
}

fn solve(n: u64) -> u64 {
    let (hay_puros_unos, k) = es_puros_unos(n);

    if hay_puros_unos {
        caso_puros_unos(k)
    } else {
        2_u64.pow(k) * 2 - 1
    }
}

fn main() {
    let q: u64 = read();

    for _ in 0..q {
        println!("{}", solve(read()));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}
