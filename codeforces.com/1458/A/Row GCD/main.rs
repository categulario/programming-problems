use std::fmt;
use std::str::FromStr;
use std::io;

fn gcd(a: u64, b: u64) -> u64 {
    if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}

fn solve(b: u64, a: &[u64]) -> u64 {
    let mut gcd_so_far = a[0] + b;

    for current in a.iter().skip(1).map(|a| a + b) {
        gcd_so_far = gcd(gcd_so_far, current);

        if gcd_so_far == 1 {
            return 1;
        }
    }

    gcd_so_far
}

fn main() {
    let _: Vec<u64> = read_vec();
    let mut a_s: Vec<u64> = read_vec();
    let b_s: Vec<u64> = read_vec();

    a_s.sort_unstable();

    for b in b_s {
        print!("{} ", solve(b, &a_s));
    }

    println!();
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
