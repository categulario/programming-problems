use std::fmt;
use std::str::FromStr;
use std::io;

#[derive(Debug)]
struct Item {
    diff: u64,
    cost: u64,
}

fn solve(mut arreglo: Vec<Item>, k: u64, mut k_p: u64, mut costo: u64) -> u64 {
    // ordenar arreglo por costo ascendente
    arreglo.sort_unstable_by_key(|i| i.cost);

    for item in arreglo {
        let falta = k - k_p;

        if falta > 0 {
            if falta < item.diff {
                costo += falta * item.cost;
                // k_p += falta;
                break;
            } else {
                costo += item.diff * item.cost;
                k_p += item.diff;
            }
        } else {
            break;
        }
    }

    costo
}

fn main() {
    let nk: Vec<u64> = read_vec();
    let n = nk[0];
    let k = nk[1];
    let mut arreglo = Vec::new();
    let mut max = 0;
    let mut min = 0;
    let mut c_min = 0;

    for _ in 0..n {
        let abc: Vec<u64> = read_vec();
        let (a, b, c) = (abc[0], abc[1], abc[2]);

        min += a;
        max += b;
        c_min += a * c;

        arreglo.push(Item {
            diff: b - a,
            cost: c,
        });
    }

    if k > max || k < min {
        println!("-1");
    } else {
        println!("{}", solve(arreglo, k, min, c_min));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read_vec<T: FromStr>() -> Vec<T>
where <T as FromStr>::Err: fmt::Debug
{
    read_line().split(" ").map(|x| x.parse().unwrap()).collect()
}
