use std::str::FromStr;
use std::fmt;
use std::io;

const MODULO: u64 = 1000000007;

fn main() {
    let n: u64 = read();
    let mut k = 0;

    for i in 2..(n+1) {
        if i%2 == 0 {
            k = (3 * (k + 1)) % MODULO;
        } else {
            k = (3 * (k - 1)) % MODULO;
        }
    }

    println!("{}", k);
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}
