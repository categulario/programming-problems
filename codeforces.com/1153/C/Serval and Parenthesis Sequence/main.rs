use std::fmt;
use std::str::FromStr;
use std::io;

fn desde_izquerda(mut cadena: Vec<char>, mut c_abren: u64, mut c_cierran: u64) -> Option<Vec<char>> {
    let mut suma = 0;
    let mitad = (cadena.len() / 2) as u64;
    let len = cadena.len();

    for (i, c) in cadena.iter_mut().enumerate() {
        if *c == '(' {
            suma += 1;
        } else if *c == ')' {
            suma -= 1;

            // suma 0 enmedio de la cadena
            if suma == 0 && i < len - 1 {
                return None;
            }
        } else {
            // consideraciones de la suma 0
            // la suma es 1 y estamos enmedio
            if suma == 1 && i < len - 1 {
                // hay que poner uno que abre, si hay
                if c_abren < mitad {
                    *c = '(';
                    c_abren += 1;
                    suma += 1;
                } else {
                    return None;
                }
            } else if i == len - 1 {
                // estamos al final, hay que poner uno que cierra, pero ¿podemos?
                if suma > 1 || c_cierran == 0 {
                    return None;
                } else {
                    *c = ')';
                    return Some(cadena);
                }
            } else {
                // ponemos del que menos hay
                if c_abren <= c_cierran {
                    *c = '(';
                    c_abren += 1;
                    suma += 1;
                } else {
                    *c = ')';
                    c_cierran += 1;
                    suma -= 1;
                }
            }
        }
    }

    if suma != 0 {
        None
    } else {
        Some(cadena)
    }
}

fn voltear(cadena: &mut [char]) {
    for c in cadena.iter_mut() {
        if *c == '(' {
            *c = ')';
        } else {
            *c = '(';
        }
    }
}

fn desde_derecha(mut cadena: Vec<char>, c_abren: u64, c_cierran: u64) -> Option<Vec<char>> {
    voltear(&mut cadena);

    desde_izquerda(cadena, c_abren, c_cierran)
}

fn solve(cadena: Vec<char>) -> Option<Vec<char>> {
    if cadena.len() % 2 == 1 {
        return None;
    }

    if *cadena.first().unwrap() == ')' {
        return None;
    }

    if *cadena.last().unwrap() == '(' {
        return None;
    }

    let mitad = cadena.len() / 2;
    let mut c_abren = 0;
    let mut c_cierran = 0;

    for c in cadena.iter() {
        if *c == '(' {
            c_abren += 1;
        } else if *c == ')' {
            c_cierran += 1;
        }
    }

    // lo de las mitades
    if c_abren > mitad || c_cierran > mitad {
        return None;
    }

    if let Some(sol) = desde_izquerda(cadena.clone(), c_abren as u64, c_cierran as u64) {
        return Some(sol);
    }

    desde_derecha(cadena, c_abren as u64, c_cierran as u64)
}

fn main() {
    let _l: u64 = read();
    let cadena: Vec<char> = read_line().chars().collect();

    if let Some(sol) = solve(cadena) {
        for c in sol {
            print!("{}", c);
        }

        println!();
    } else { // devolvió None
        println!(":(");
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read<T: FromStr>() -> T
where <T as FromStr>::Err: fmt::Debug
{
    read_line().parse().unwrap()
}
