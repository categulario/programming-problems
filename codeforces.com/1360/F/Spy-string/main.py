from typing import List, Dict, Tuple, Set, Iterable


def mas_popular(d: Dict[str, int]) -> str:
    return sorted(d.items(), key=lambda x: x[1], reverse=True)[0][0]


def distancia(cadena: str, otra: str) -> int:
    dist = 0

    for a, b in zip(cadena, otra):
        if a != b:
            dist += 1

    return dist


def make_num(indices: List[int], maps: List[List[str]]) -> str:
    res = ''
    for i, m in zip(indices, maps):
        res += m[i]
    return res


def is_zero(indices: List[int]) -> bool:
    for i in indices:
        if i != 0:
            return False

    return True


def add_one(indices: List[int], maps: List[List[str]]) -> List[int]:
    indices = indices[:]
    i = len(indices) - 1
    indices[i] += 1

    while indices[i] == len(maps[i]): # hay overflow
        indices[i] = 0

        if i > 0:
            indices[i-1] += 1
            i -= 1

    return indices


def todas_las_combinaciones(maps: List[Set[str]]) -> Iterable[str]:
    maps = list(map(list, maps))
    indices = [0 for _ in range(len(maps))]

    yield make_num(indices, maps)

    while True:
        indices = add_one(indices, maps)

        if is_zero(indices):
            break

        yield make_num(indices, maps)


def solve(cadenas: List[str]):
    maps = [set() for _ in cadenas[0]]

    for cadena in cadenas:
        for caracter, conjunto in zip(cadena, maps):
            conjunto.add(caracter)

    mejor_maxima_distancia = 100000000
    mejor_cadena = ''

    for posible in todas_las_combinaciones(maps):
        maxima = 0
        for cadena in cadenas:
            cur = distancia(posible, cadena)
            if cur > maxima:
                maxima = cur

        if maxima < mejor_maxima_distancia:
            mejor_maxima_distancia = maxima
            mejor_cadena = posible

    if mejor_maxima_distancia > 1:
        return -1

    return mejor_cadena


if __name__ == '__main__':
    t = int(input())

    for i in range(t):
        n, m = list(map(int, input().strip().split()))
        cadenas = [input() for _ in range(n)]

        print(solve(cadenas))
