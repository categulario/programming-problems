use std::fmt;
use std::str::FromStr;
use std::io;

fn solucion(arreglo: &mut [u32]) -> bool {
    // 1. ordenar el arreglo
    arreglo.sort();

    // 2. recorremos para buscar al menos un par de elementos a distancia 1
    let hay_dos_cercanos = arreglo
        .iter()
        .zip(&arreglo[1..]) // <----- a que se queja
        .map(|(a, b)| b - a)
        .filter(|d| *d == 1)
        .count() > 0;

    // 3. contamos la cantidad de pares e impares
    let pares = arreglo
        .iter()
        .map(|a| a % 2)
        .filter(|m| *m == 0)
        .count();

    // 4. si la cantidad de pares (y por ende de impares) es par
    if pares % 2 == 0 {
        true
    } else {  // si no (sobra uno)
        //      si había un par de elementos a distancia 1
        //          true
        //      si no
        //          false
        hay_dos_cercanos
    }
}

fn main() {
    let t: u32 = read();  // leer el número de casos

    for _ in 0..t {  // por cada caso
        let _: u32 = read();  // el número de elementos del arreglo
        let mut arreglo: Vec<u32> = read_vec();

        if solucion(&mut arreglo) {
            println!("YES");
        } else {
            println!("NO");
        }
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
