use std::fmt;
use std::str::FromStr;
use std::io;

fn solucion(cadena: String, k: u64) -> String {
    // ordeno las letras
    let mut caracteres: Vec<_> = cadena.chars().collect();

    caracteres.sort();

    // si k es 1 devuélvelas ordenadas
    if k == 1 {
        return caracteres.into_iter().collect();
    }

    // ver cómo se reparte el primer bloque de letras iguales
    let primer_letra = caracteres[0];
    let mut letras_iguales = 0;
    while letras_iguales < caracteres.len() && caracteres[letras_iguales] == primer_letra {
        letras_iguales += 1;
    }

    if (letras_iguales as u64) < k {
        // no alcanzan
        caracteres[k as usize].into()
    } else {
        // si alzanzan
        let mut respuesta = String::new();
        respuesta.push(primer_letra);

        respuesta.extend(&caracteres[(k as usize)..]);

        return respuesta;
    }
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        let n_k: Vec<u64> = read_vec();
        let cadena = read_line().trim().into();

        println!("{}", solucion(cadena, n_k[1]));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
