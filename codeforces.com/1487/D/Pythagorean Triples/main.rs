use std::fmt;
use std::str::FromStr;
use std::io;

/*
struct Estado {
    cache: Vec<u64>,
}

impl Estado {
    fn new() -> Estado {
        Estado {
            cache: Vec::new(),
        }
    }

    fn query(&mut self, n: u64) -> u64 {
        if let Some(last) = self.cache.last() {
            if n > *last {
                self.compute(n)
            } else {
                self.binary_search(n)
            }
        } else {
            self.compute(n)
        }
    }

    fn compute(&mut self, n: u64) -> u64 {
        let last_c = if let Some(last) = self.cache.last() {
            *last
        } else {
            1
        };

        pass
    }

    fn binary_search(&self, n: u64) -> u64 {
        unimplemented!()
    }
}
*/

fn solve(n: u64) -> u64 {
    ((-1.0 + ((2 * n - 1) as f64).sqrt().floor()) / 2.0) as u64
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        let n: u64 = read();

        println!("{}", solve(n));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}
