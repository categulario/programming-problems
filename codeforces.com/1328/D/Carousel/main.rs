use std::fmt;
use std::str::FromStr;
use std::io;

struct ColoredFigure {
    how_many: usize,
    color: u64,
    figure: u64,
}

impl ColoredFigure {
    fn new(how_many: usize, color: u64, figure: u64) -> ColoredFigure {
        ColoredFigure {
            how_many, color, figure,
        }
    }

    fn to_string(self) -> String {
        (0..self.how_many).map(|_| self.color.to_string()).collect::<Vec<_>>().join(" ")
    }
}

fn solve(carousel: Vec<u64>) -> (usize, Vec<ColoredFigure>) {
    let mut colors = 1;
    let mut curr_figure = carousel[0];
    let mut curr_color = 1;
    let mut has_increased = false;
    let mut answer = vec![ColoredFigure::new(1, 1, carousel[0])];

    for figure in carousel.into_iter().skip(1) {
        if figure == curr_figure {
            if let Some(last) = answer.last_mut() {
                last.how_many += 1;
            }
        } else {
            if curr_color == 1 {
                if !has_increased {
                    colors += 1;
                    has_increased = true;
                }

                curr_color = 2;
            } else {
                curr_color = 1;
            }

            answer.push(ColoredFigure::new(1, curr_color, figure));
            curr_figure = figure;
        }
    }

    if answer.last().unwrap().color == answer.first().unwrap().color &&
        answer.last().unwrap().figure != answer.first().unwrap().figure {
        let last = answer.last_mut().unwrap();

        last.color = 3;
        colors += 1;
    }

    (colors, answer)
}

fn main() {
    let q: u64 = read();

    for _ in 0..q {
        let _n: u64 = read();
        let carousel: Vec<u64> = read_vec();

        let (k, answer) = solve(carousel);

        println!("{}", k);
        println!("{}", answer.into_iter().map(|b| b.to_string()).collect::<Vec<_>>().join(" "));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
