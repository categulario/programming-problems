#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

void solucion(int n, string numero) {
    string res_a; // este es el mayor
    string res_b;

    bool es_igual = true;

    for (int i = 0; i < n; i++) {
        if (es_igual) {
            if (numero[i] == '2') {
                res_a.push_back('1');
                res_b.push_back('1');
            } else if (numero[i] == '0') {
                res_a.push_back('0');
                res_b.push_back('0');
            } else { // nos encontramos un 1
                es_igual = false;

                res_a.push_back('1');
                res_b.push_back('0');
            }
        } else {
            if (numero[i] == '2') {
                res_a.push_back('0');
                res_b.push_back('2');
            } else if (numero[i] == '0') {
                res_a.push_back('0');
                res_b.push_back('0');
            } else { // nos encontramos un 1
                res_a.push_back('0');
                res_b.push_back('1');
            }
        }
    }

    cout << res_a << endl;
    cout << res_b << endl;
}

int main() {
    int t;
    int n;
    string numero;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        scanf("%d", &n);

        cin.ignore();

        getline(cin, numero);

        solucion(n, numero);
    }
}
