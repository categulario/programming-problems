use std::fmt;
use std::str::FromStr;
use std::io;

fn mcd(a: i64, b: i64) -> i64 {
    if b == 0 {
        a
    } else {
        mcd(b, a % b)
    }
}

fn solve(a: Vec<i64>) -> Vec<i64> {
    let mut a_: Vec<(usize, i64)> = a.into_iter().enumerate().collect();

    a_.sort_unstable_by_key(|(_, v)| v.abs());

    let chunks = a_.chunks_exact(2);
    let last = a_.len() / 2;
    let rem = chunks.remainder().get(0);

    let mut ans: Vec<_> = chunks.enumerate().map(|(i, chunk)| {
        if i == last - 1 {
            if let Some(rem) = rem {
                let a = chunk[1].1 * rem.1;
                let b = chunk[0].1 * rem.1;
                let c = -2 * chunk[0].1 * chunk[1].1;

                let m = mcd(a, mcd(b, c));

                return vec![
                    (chunk[0].0, a / m),
                    (chunk[1].0, b / m),
                    (rem.0, c / m),
                ]
            }
        }

        let a = chunk[1].1;
        let b = -chunk[0].1;
        let m = mcd(a, b);

        vec![
            (chunk[0].0, a/m),
            (chunk[1].0, b/m),
        ]
    }).flatten().collect();

    ans.sort_unstable_by_key(|&(i, _)| i);

    ans.into_iter().map(|(_, v)| v).collect()
}

fn main() {
    let t: i64 = read();

    for _ in 0..t {
        let _n: i64 = read();
        let a: Vec<i64> = read_vec();

        let sol = solve(a);

        println!("{}", sol.into_iter().map(|b| b.to_string()).collect::<Vec<_>>().join(" "));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read<T: FromStr>() -> T
where <T as FromStr>::Err: fmt::Debug
{
    read_line().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
where <T as FromStr>::Err: fmt::Debug
{
    read_line().split(" ").map(|x| x.parse().unwrap()).collect()
}
