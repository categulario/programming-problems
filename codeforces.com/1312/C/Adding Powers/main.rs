use std::io;
use std::fmt;
use std::str::FromStr;

fn case(_n: u64, k: u64, mut list: Vec<u64>) -> bool {
    let mut digits: Vec<u64>;

    loop {
        if list.iter().all(|x| x == &0_u64) {
            break;
        }

        digits = list.iter().map(|x| x % k).collect();
        list = list.into_iter().map(|x| x / k).collect();

        if digits.iter().sum::<u64>() > 1 {
            return false;
        }
    }

    true
}

fn main() {
    let num_cases: u64 = read();

    for _i in 0..num_cases {
        let (n, k) = read_tuple();
        let list = read_vec();

        if case(n, k, list) {
            println!("YES");
        } else {
            println!("NO");
        }
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}

fn read_tuple<T: FromStr+Copy>() -> (T, T)
    where <T as FromStr>::Err: fmt::Debug
{
    let vec: Vec<T> = read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect();

    (vec[0], vec[1])
}
