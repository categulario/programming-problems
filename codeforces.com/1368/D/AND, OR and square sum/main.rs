use std::fmt;
use std::str::FromStr;
use std::io;

fn solve(mut nums: Vec<u64>) -> u64 {
    if nums.len() == 1 {
        return nums[0].pow(2);
    }

    for i in 0..(nums.len() - 1) {
        for j in i..nums.len() {
            let or = nums[i] | nums[j];
            let and = nums[i] & nums[j];

            nums[i] = or;
            nums[j] = and;
        }
    }

    nums.into_iter().map(|i| i.pow(2)).sum()
}

fn main() {
    let _n: u64 = read();
    let nums: Vec<u64> = read_vec();

    println!("{}", solve(nums));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
