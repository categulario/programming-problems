use std::fmt;
use std::str::FromStr;
use std::io;

fn solve(salsas: Vec<u64>) -> Vec<bool> {
    let mut hotcakes = vec![false; salsas.len()];
    let mut salsa = 0;

    for i in (0..salsas.len()).rev() {
        salsa = salsas[i].max(salsa);

        if salsa > 0 {
            hotcakes[i] = true;
            salsa -= 1;
        }
    }

    hotcakes
}

fn present(hotcakes: Vec<bool>) -> String {
    hotcakes.into_iter().map(|h| if h { "1" } else { "0" }).collect::<Vec<_>>().join(" ")
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        let _n: u64 = read();
        let salsas: Vec<u64> = read_vec();

        println!("{}", present(solve(salsas)));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read<T: FromStr>() -> T
where <T as FromStr>::Err: fmt::Debug
{
    read_line().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
where <T as FromStr>::Err: fmt::Debug
{
    read_line().split(" ").map(|x| x.parse().unwrap()).collect()
}
