use std::fmt;
use std::str::FromStr;
use std::io;

fn make_beautiful(n: usize, head: &[u8]) -> Vec<u8> {
    head.iter().copied().cycle().take(n).collect()
}

fn is_bigger_or_equal(a: &[u8], b: &[u8]) -> bool {
    a
        .iter().zip(b.iter())
        .all(|(&a, &b)| {
            a >= b
        })
}

fn inc(num: &[u8]) -> Vec<u8> {
    let mut sol = num.to_vec();
    let mut i = num.len() - 1;

    loop {
        if sol[i] + 1 == 10 {
            sol[i] = 0;
            i -= 1;
        } else {
            sol[i] += 1;
            break;
        }
    }

    sol
}

fn increment(head: &[u8], n: usize) -> Vec<u8> {
    let new_head = inc(head);

    make_beautiful(n, &new_head)
}

fn solve(n: Vec<u8>, k: usize) -> Vec<u8> {
    let sol = make_beautiful(n.len(), &n[..k]);

    if is_bigger_or_equal(&sol, &n) {
        sol
    } else {
        increment(&sol[..k], n.len())
    }
}

fn print_sol(sol: Vec<u8>) {
    println!("{}", sol.len());

    for digit in sol {
        print!("{}", digit);
    }

    println!();
}

fn main() {
    let nk: Vec<_> = read_vec();
    let k = nk[1];
    let n = read_line();

    let sol = solve(n.chars().map(|c| c.to_digit(10).unwrap() as u8).collect(), k);

    print_sol(sol);
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
