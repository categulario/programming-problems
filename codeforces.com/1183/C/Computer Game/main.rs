use std::fmt;
use std::str::FromStr;
use std::io;
use std::collections::HashMap;

// m: representa la cantidad de a's que tenemos
fn binaria(k: i64, n: i64, m: i64, delta: i64, a: i64) -> i64 {
    if m == 0 {
        return -1;
    }

    if /* m es lo mejor que puede ser */ {
        // si recuperamos una a ya no se termina el juego y
        // si convertimos una a más el resultado es subóptimo
        //
        // seguramente tiene que ver con delta = 0
        return m;
    }
}

fn resolver(k: i64, n: i64, a: i64, b: i64) -> i64 {
    let m = (k - 1) / a;
    let r = (k - 1) % a + 1;

    if m >= n {
        n
    } else {
        if (n - m) * b < r - 1 {
            m
        } else { // Faltan b's y tenemos algunas a's
            binaria(k, n, m/2, m/2, a, b)
        }
    }
}

fn main() {
    let q: i64 = read();

    for _i in 0..q {
        let datos: Vec<i64> = read_vec();

        let k = datos[0];
        let n = datos[1];
        let a = datos[2];
        let b = datos[3];

        println!("{}", resolver(k, n, a, b));
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
