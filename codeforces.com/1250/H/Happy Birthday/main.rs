use std::fmt;
use std::str::FromStr;
use std::io;

enum Caso {
    HayMenosCeros(u64),
    HayMenosI(u8, u64),
}

fn caso(nums: &[u64]) -> Caso {
    let mut menor_i: u8 = 0;
    let mut c_i = u64::MAX;

    for i in 0..10 {
        if nums[i] < c_i {
            menor_i = i as u8;
            c_i = nums[i];
        }
    }

    if menor_i == 0 {
        for i in 1..10 {
            if nums[i] == c_i {
                menor_i = i as u8;

                return Caso::HayMenosI(menor_i, c_i);
            }
        }

        Caso::HayMenosCeros(c_i)
    } else {
        Caso::HayMenosI(menor_i, c_i)
    }
}

fn solve(nums: &[u64]) {
    let c = caso(nums);

    match c {
        Caso::HayMenosCeros(n) => {
            print!("1");

            for _ in 0..=n {
                print!("0");
            }

            println!();
        },
        Caso::HayMenosI(i, n) => {
            for _ in 0..(n + 1) {
                print!("{}", i);
            }

            println!();
        },
    }
}

fn main() {
    let t: u64 = read();

    for _ in 0..t {
        let nums: Vec<u64> = read_vec();

        solve(&nums);
    }
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n
}

fn read<T: FromStr>() -> T
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().parse().unwrap()
}

fn read_vec<T: FromStr>() -> Vec<T>
    where <T as FromStr>::Err: fmt::Debug
{
    read_line().trim().split(" ").map(|x| x.trim().parse().unwrap()).collect()
}
