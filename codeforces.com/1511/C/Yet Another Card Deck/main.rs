use std::fmt;
use std::str::FromStr;
use std::io;
use std::collections::HashMap;

fn solve(ns: Vec<u64>, qs: Vec<u64>) -> Vec<u64> {
    let mut positions: HashMap<u64, u64> = HashMap::new();

    for (i, n) in ns.into_iter().enumerate() {
        if positions.contains_key(&n) {
            continue;
        }

        positions.entry(n).or_insert(i as u64 + 1);
    }

    let mut salida = Vec::with_capacity(qs.len());

    for q in qs {
        let index = *positions.get(&q).unwrap();

        salida.push(index);

        // actualización
        for v in positions.values_mut() {
            if *v < index {
                *v = *v + 1;
            }
        }

        let entry = positions.get_mut(&q).unwrap();

        *entry = 1;
    }

    salida
}

fn present(s: Vec<u64>) -> String {
    s.into_iter().map(|c| c.to_string()).collect::<Vec<_>>().join(" ")
}

fn main() {
    let _nq: Vec<u64> = read_vec();
    let ns: Vec<u64> = read_vec();
    let qs: Vec<u64> = read_vec();

    let solution = solve(ns, qs);

    println!("{}", present(solution));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().to_string()
}

fn read_vec<T: FromStr>() -> Vec<T>
where <T as FromStr>::Err: fmt::Debug
{
    read_line().split(" ").map(|x| x.parse().unwrap()).collect()
}
