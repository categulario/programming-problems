use std::io;
use std::collections::HashMap;

struct Solution {}

fn es_valido_1(c: char) -> bool {
    c != '0'
}

fn es_valido_2(s: &[char]) -> bool {
    if s[0] == '0' {
        return false;
    }

    let num = s[0].to_digit(10).unwrap() * 10 + s[1].to_digit(10).unwrap();

    num <= 26
}

fn how_many_ways<'a>(s: &'a [char], cache: &mut HashMap<&'a [char], i32>) -> i32 {
    if cache.contains_key(s) {
        return *cache.get(s).unwrap();
    }

    let ans = match s.len() {
        1 => {
            // checa si es válido, devuelve 1 o 0
            if es_valido_1(s[0]) { 1 } else { 0 }
        },

        2 => {
            // checa si es válido, acumula 1 o 0
            // recursivamente llama con el último caracter
            // suma ambas respuestas
            // devuelve la suma
            (if es_valido_2(s) {
                1
            } else {
                0
            } + if es_valido_1(s[0]) {
                how_many_ways(&s[1..], cache)
            } else {
                0
            })
        },

        _ => {
            // la longitud es más de dos
            // toma un caracter y llama recursivamente con el resto
            // toma dos caracteres y llama recursivamente con el resto
            // devuelve la suma de las dos llamadas recusivas
            let tomando_1 = if es_valido_1(s[0]) {
                how_many_ways(&s[1..], cache)
            } else {
                0
            };
            let tomando_2 = if es_valido_2(&s[..2]) {
                how_many_ways(&s[2..], cache)
            } else {
                0
            };

            tomando_1 + tomando_2
        },
    };

    cache.insert(s, ans);

    ans
}

impl Solution {
    pub fn num_decodings(s: String) -> i32 {
        let mut c = HashMap::new();
        let chars: Vec<_> = s.chars().collect();

        how_many_ways(&chars, &mut c)
    }
}

fn main() {
    let s = read_line();

    println!("{}", Solution::num_decodings(s));
}

fn read_line() -> String {
    let mut pre_n = String::new();
    io::stdin().read_line(&mut pre_n).unwrap();
    pre_n.trim().into()
}
